import requests
import json
from time import sleep

cities = set()
with open('../yahoo/yahoo_scrape.json') as companies:
    data = json.load(companies)
    for c in data:
        if 'summaryProfile' in c:
            if 'city' in c['summaryProfile']:
                cities.add(c['summaryProfile']['city'])

print('Processing ' + str(len(cities)) + ' cities...')

def getCityGeoIdUrl(name):
    url = f'https://api.teleport.org/api/cities/?search={name}&limit=1'
    resp = requests.get(url)
    if resp.status_code == 200:
        data = resp.json()
        try:
            return data['_embedded']['city:search-results'][0]['_links']['city:item']['href']
        except Exception as e:
            print(f'Error in getCityGeoIdUrl({name}):')
            print(e)
            return None
    else:
        print(f'Bad status code for getCityGeoIdUrl({name}): {resp.status_code}')
        return None

def getCityScores(url):
    try:
        resp = requests.get(url)
        if resp.status_code == 200:
            data = resp.json()
            try:
                scores_resp = requests.get(data['_links']['ua:scores']['href'])
                return scores_resp.json()
            except Exception as e:
                print(f'Error getting {url}:')
                print(e)
                return None
        else:
            print(f'Bad status code for GET {url}: {resp.status_code}')
            return None
    except Exception as e:
        print(e)

def getCityDetails(url):
    resp = requests.get(url)
    if resp.status_code == 200:
        data = resp.json()
        try:
            data['scores'] = getCityScores(
                data['_links']['city:urban_area']['href'])
            return data
        except Exception as e:
            print(f'Error getting {url}:')
            print(e)
            return None
    else:
        print(f'Bad status code for GET {url}: {resp.status_code}')
        return None

output = []

for c in cities:
    print(f'Processing {c}...', end='', flush=True)
    geoid_url = getCityGeoIdUrl(c)
    sleep(0.5)
    if geoid_url:
        city_data = getCityDetails(geoid_url)
        output.append(city_data)
        print('done', flush=True)
    sleep(0.5) # avoid rate limits

with open('cities_scrape.json', 'w') as outfile:
    json.dump(output, outfile)
