import psycopg2
import json
import re

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="tfxI1nb494hjgy1j", host="35.184.227.230")

cur = conn.cursor()

def parse_fullname(s):
    if "D.C." in s:
        return ("Washington D.C.", "Washington D.C.", "United States")

    return (p.strip() for p in s.split(','))

def insert_city(c):
    id = c["id"]
    name = c["name"]
    summary = re.sub('<[^<]+?>', '', c["summary"])
    full_name = c["full_name"]
    population = c["population"]
    overallscore = c["overall_score"]
    metrics = json.dumps(c["metrics"])
    latlon = json.dumps(c["latlon"])

    _, state, country = parse_fullname(full_name)
    cur.execute(
        """
        INSERT INTO cities (id, name, full_name, latlon, population, metrics, summary, overallscore, country, state)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """,
        (id, name, full_name, latlon, population, metrics, summary, overallscore, country, state)
    )
    print(f'Imported {full_name}')

with open('cities_clean.json') as cities_file:
    cities = json.load(cities_file)

    for c in cities:
        insert_city(c)

    conn.commit()

