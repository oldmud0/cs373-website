import psycopg2
import json

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="tfxI1nb494hjgy1j", host="35.184.227.230")

cur = conn.cursor()

def import_company(c):
    id = c["id"]
    name = c["name"],
    market_cap = c["market_cap"]
    symbol = c["symbol"]
    esgscores = json.dumps(c["esg_scores"])

    summary = c["summary"]
    industry = summary.get("sector", None)
    n_employees = summary.get("fullTimeEmployees", None)
    text_summary = summary.get("longBusinessSummary", None)
    website = summary.get("website", None)
    address = summary.get("formatted_address", None)
    location = json.dumps(summary.get("location", None))

    cur.execute(
        """
        SELECT id FROM cities WHERE name = %s;
        """,
        (summary.get("city", None),)
    )

    city_id = cur.fetchone() or None

    cur.execute(
        """
        INSERT INTO companies (id, name, market_cap, symbol, esg_scores, n_employees, industry, summary, address, location, city_id, website)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """,
        (id, name, market_cap, symbol, esgscores, n_employees, industry, text_summary, address, location, city_id, website,)
    )

    print(f'Inserted {name}')

with open('companies_cleaned.json') as companies_file:
    companies = json.load(companies_file)

    for c in companies:
        import_company(c)

    conn.commit()

