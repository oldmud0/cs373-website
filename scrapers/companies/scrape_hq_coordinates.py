import requests
import os
import json
from sys import exit

GOOGLE_API_KEY = os.environ.get('GOOGLE_API_KEY')

if not GOOGLE_API_KEY:
    print('Environment variable GOOGLE_API_KEY not set')
    exit(1)

def getStreetAddress(company):
    try:
        summary = company['summaryProfile']
        address = summary.get('address1', '')
        city = summary.get('city', '')
        state = summary.get('state', '')
        country = summary.get('country', '')
        return f'{address}, {city}, {state}, {country}'
    except Exception as e:
        print('Error in getting street address:')
        print(e)
        return ''

GOOGLE_GEOCODING_BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?address='

data = []
with open('yahoo_scrape.json') as scrapefile_1:
    data = json.load(scrapefile_1)
with open('yahoo_scrape2.json') as scrapefile_2:
    data.extend(json.load(scrapefile_2))

for c in data:
    try:
        print('Scraping data for ' + c['quoteType']['longName'] + '...', end='', flush=True)
        address = getStreetAddress(c)
        if address:
            resp = requests.get(GOOGLE_GEOCODING_BASE_URL + address + '&key=' + GOOGLE_API_KEY)
            resp_data = resp.json()
            result = resp_data['results'][0]

            c['summaryProfile']['formatted_address'] = result['formatted_address']
            location = result['geometry']['location']
            c['summaryProfile']['location'] = {
                'latitude': location['lat'],
                'longitude': location['lng']
            }
            print('done', flush=True)
            print(c['summaryProfile']['location'])
    except Exception as e:
        print(e)

with open('yahoo_scrape_with_coords.json', 'w') as outfile:
    json.dump(data, outfile)
