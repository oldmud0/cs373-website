import psycopg2
import json
from uuid import uuid4

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="tfxI1nb494hjgy1j", host="35.184.227.230")

cur = conn.cursor()

INSERTED_OPERATORS = {}

def query_city(city):
    print(f'Looking for city {city}')
    cur.execute(
        """
        SELECT id FROM cities WHERE name = %s;
        """,
        (city,)
    )

    city_id = cur.fetchone() or None
    return city_id

def import_operator(o):
    if not o:
        return None, None

    id = uuid4()
    geometry = json.dumps(o["geometry"])
    onestop_id = o["onestop_id"]
    tags = json.dumps(o["tags"])
    name = o["name"]
    short_name = o["short_name"]
    website = o["website"]

    city_id = query_city(o["metro"])

    cur.execute(
        """
        INSERT INTO operators (id, onestop_id, tags, name, short_name, website, city_id, geometry)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
        """,
        (str(id), onestop_id, tags, name, short_name, website, city_id, geometry,)
    )
    print(f'Imported operator {name}')

    return str(id), city_id


def import_route(r):
    id = r.get("id", uuid4())
    onestop_id = r.get("onestop_id", None)
    name = r.get("name", None)
    vehicle_type = r.get("vehicle_type", None)
    stops = json.dumps(r.get("stops", []))
    bikes_allowed = r.get("bikes_allowed", None)
    wheelchair_accessible = r.get("wheelchair_accessible", None)
    geometry = json.dumps(r.get("geometry", []))

    operator = r["operator"]
    operator_onestop = operator["onestop_id"]
    operator_id = None
    city_id = None

    if operator_onestop in INSERTED_OPERATORS:
        operator_id, city_id = INSERTED_OPERATORS[operator_onestop]
    else:
        operator_id, city_id = import_operator(r.get("operator", None))
        INSERTED_OPERATORS[operator_onestop] = (operator_id, city_id)

    if not operator_id:
        print(f'Warning: route with no operator found ({id})')
    if not city_id:
        print(f'Warning: route with no city found ({id})')

    cur.execute(
        """
        INSERT INTO routes (id, onestop_id, name, vehicle_type, stops, bikes_allowed, wheelchair_accessible, geometry, operator_id, city_id)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """,
        (str(id), onestop_id, name, vehicle_type, stops, bikes_allowed, wheelchair_accessible, geometry, operator_id, city_id,)
    )
    print(f'Imported route {onestop_id} ({name})')

with open('routes_cleaned.json') as routes_file:
    routes = json.load(routes_file)

    for r in routes:
        import_route(r)

    conn.commit()
