pm.test("response is ok", function () {
    pm.response.to.have.status(200);
});

pm.test("environment to be production", function () {
    pm.expect(pm.environment.get("env")).to.equal("production");
});

pm.test("response should be okay to process", function () {
    pm.response.to.not.be.error;
    pm.response.to.not.have.jsonBody("error");
});

pm.test("response return a correct item with the id we specified", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData.id).to.eql(pm.environment.get("companyid"));
});


// example using pm.response.to.be*
pm.test("response must be valid and have a body", function () {
     // assert that the status code is 200
     pm.response.to.be.ok; // info, success, redirection, clientError,  serverError, are other variants
     // assert that the response has a valid JSON body
     pm.response.to.be.withBody;
     pm.response.to.be.json; // this assertion also checks if a body  exists, so the above check is not needed
});