// Set the page size
pm.environment.set("size", 4);
pm.environment.set("page", 5);

pm.test("response is ok", function () {
    pm.response.to.have.status(200);
});

pm.test("environment to be production", function () {
    pm.expect(pm.environment.get("env")).to.equal("production");
});

pm.test("response should be okay to process", function () {
    pm.response.to.not.be.error;
    pm.response.to.have.jsonBody("content");
    pm.response.to.not.have.jsonBody("error");
});

// Test if the page number equals to the number we specified before
pm.test("Test page id", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData.number).to.eql(pm.environment.get("page"));
});

// Test if the page size equals to the size we specified before
pm.test("Test page size", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData.size).to.eql(pm.environment.get("size"));
});

// example using pm.response.to.be*
pm.test("response must be valid and have a body", function () {
     // assert that the status code is 200
     pm.response.to.be.ok; // info, success, redirection, clientError,  serverError, are other variants
     // assert that the response has a valid JSON body
     pm.response.to.be.withBody;
     pm.response.to.be.json; // this assertion also checks if a body  exists, so the above check is not needed
});