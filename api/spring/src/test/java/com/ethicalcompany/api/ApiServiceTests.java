package com.ethicalcompany.api;

import com.ethicalcompany.api.domain.city.City;
import com.ethicalcompany.api.domain.company.Company;
import com.ethicalcompany.api.domain.route.Route;
import com.ethicalcompany.api.service.city.CityService;
import com.ethicalcompany.api.service.company.CompanyService;
import com.ethicalcompany.api.service.route.RouteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.lang.IllegalArgumentException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiServiceTests {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private CityService cityService;
    @Autowired
    private RouteService routeService;

    @Test
    public void testPageRequestOutOfBoundsPage() {
        try {
            Pageable pageable = new PageRequest(-1, 1);
            assert false;
        } catch(IllegalArgumentException e) {
            assert true;
        }
    }

    @Test
    public void testPageRequestOutOfBoundsAmount() {
        try {
            Pageable pageable = new PageRequest(1, -1);
            assert false;
        } catch(IllegalArgumentException e) {
            assert true;
        }
    }



    @Test
    public void testCompanyServicePaging() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(2, 5, sort);
        Page<Company> companyPage = companyService.findAllCompany(null, pageable);
        List<Company> companyList = companyPage.getContent();

        assert !companyPage.isEmpty();
        assert companyList.size() == 5;
    }

    @Test
    public void testCompanyServiceGetCompany() {
        Pageable pageable = new PageRequest(2, 2);
        Page<Company> companyPage = companyService.findAllCompany(null, pageable);
        List<Company> companyList = companyPage.getContent();

        String companyId = companyList.get(0).getId();
        Optional<Company> company = companyService.findCompanyById(companyId);
        assert company.isPresent();
    }

    @Test
    public void testCompanyServiceSorting() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(1, 6, sort);
        Page<Company> companyPage = companyService.findAllCompany(null, pageable);
        List<Company> companyList = companyPage.getContent();

        String firstCompany = companyList.get(0).getId();
        String secondCompany = companyList.get(1).getId();

        assert firstCompany.compareTo(secondCompany) > 0;
    }



    @Test
    public void testCityServicePaging() {
        Pageable pageable = new PageRequest(3, 2);
        Page<City> cityPage = cityService.findAllCity(null, pageable);
        List<City> cityList = cityPage.getContent();

        assert !cityPage.isEmpty();
        assert cityList.size() == 2;
    }

    @Test
    public void testCityServiceGetCity() {
        Pageable pageable = new PageRequest(2, 8);
        Page<City> cityPage = cityService.findAllCity(null, pageable);
        List<City> cityList = cityPage.getContent();

        String cityId = cityList.get(0).getId();
        Optional<City> city = cityService.findCityById(cityId);
        assert city.isPresent();
    }

    @Test
    public void testCityServiceSorting() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(7, 3, sort);
        Page<City> cityPage = cityService.findAllCity(null, pageable);
        List<City> cityList = cityPage.getContent();

        String firstCity = cityList.get(0).getId();
        String secondCity = cityList.get(1).getId();

        assert firstCity.compareTo(secondCity) > 0;
    }



    @Test
    public void testRouteServicePaging() {
        Pageable pageable = new PageRequest(4, 6);
        Page<Route> routePage = routeService.findAllRoute(
                null,
                null,
                null,
                null,
                null,
                pageable
        );
        List<Route> routeList = routePage.getContent();

        assert !routePage.isEmpty();
        assert routeList.size() == 6;
    }

    @Test
    public void testRouteServiceGetRoute() {
        Pageable pageable = new PageRequest(5, 2);
        Page<Route> routePage = routeService.findAllRoute(
                null,
                null,
                null,
                null,
                null,
                pageable
        );
        List<Route> routeList = routePage.getContent();

        String routeId = routeList.get(0).getId();
        Optional<Route> route = routeService.findRouteById(routeId);
        assert route.isPresent();
    }

    @Test
    public void testRouteServiceSorting() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(1, 3, sort);
        Page<Route> routePage = routeService.findAllRoute(
                null,
                null,
                null,
                null,
                null,
                pageable
        );
        List<Route> routeList = routePage.getContent();

        String firstRoute = routeList.get(0).getId();
        String secondRoute = routeList.get(1).getId();

        assert firstRoute.compareTo(secondRoute) > 0;
    }
}
