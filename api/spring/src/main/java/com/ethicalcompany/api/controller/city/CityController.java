package com.ethicalcompany.api.controller.city;
import com.ethicalcompany.api.domain.city.City;
import com.ethicalcompany.api.domain.city.CityEndpointInfo;
import com.ethicalcompany.api.service.city.CityService;
import net.kaczmarzyk.spring.data.jpa.domain.EqualIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.domain.In;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@And({
        @Spec(path = "name", spec = LikeIgnoreCase.class),
        @Spec(path = "country", spec = In.class),
        @Spec(path = "state", spec = EqualIgnoreCase.class)
})
interface CitySpec extends Specification<City> {}

@RestController
@CrossOrigin
public class CityController {
    @Autowired
    private CityService cityService;

    @RequestMapping("/v1/city/list")
    public Page<City> getCityList(
            CitySpec spec,
            @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        return cityService.findAllCity(spec, pageable);
    }

    @RequestMapping("/v1/city/detail")
    public Optional<City> getCityDetail(
            @RequestParam(value="id", defaultValue = "1") String id) {
        return cityService.findCityById(id);
    }

    @RequestMapping("/v1/city/info")
    public CityEndpointInfo getInfo() {
        return new CityEndpointInfo();
    }

    @RequestMapping("/v1/city/search")
    public Page<City> searchCity(
            @RequestParam(value = "content", defaultValue = "Dallas") String content,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "15") Integer size) {
        Pageable pageable = new PageRequest(page, size);
        return cityService.search(content, pageable);
    }
}
