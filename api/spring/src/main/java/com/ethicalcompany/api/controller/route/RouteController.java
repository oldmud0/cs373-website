package com.ethicalcompany.api.controller.route;

import java.util.List;
import java.util.Optional;

import com.ethicalcompany.api.domain.route.Route;
import com.ethicalcompany.api.domain.route.RouteEndpointInfo;
import com.ethicalcompany.api.service.route.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class RouteController {
    @Autowired
    private RouteService routeService;

    @RequestMapping("/v1/route/list")
    public Page<Route> getRouteList(
            @RequestParam(name = "cityId", required = false) String cityId,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "vehicleType", required = false) List<String> vehicleType,
            @RequestParam(name = "bikesAllowed", required = false) List<String> bikesAllowed,
            @RequestParam(name = "wheelchairAccessible", required = false) List<String> wheelchairAccessible,
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, page = 0, size = 15) Pageable pageable
    ) {
        return routeService.findAllRoute(
                cityId,
                name,
                vehicleType,
                bikesAllowed,
                wheelchairAccessible,
                pageable
        );
    }

    @RequestMapping("/v1/route/detail")
    public Optional<Route> getRouteDetail(
            @RequestParam(value="id", defaultValue = "1") String id) {
        return routeService.findRouteById(id);
    }

    @RequestMapping("/v1/route/info")
    public RouteEndpointInfo getInfo() {
        return new RouteEndpointInfo();
    }

    @RequestMapping("/v1/route/search")
    public Page<Route> searchCompany(
            @RequestParam(value = "content", defaultValue = "Bus") String content,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "15") Integer size) {

        Pageable pageable = PageRequest.of(page, size);
        return routeService.search(content, pageable);
    }
}