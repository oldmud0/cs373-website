package com.ethicalcompany.api.service.company;

import com.ethicalcompany.api.domain.company.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;

public interface CompanyService {
    Page<Company> findAllCompany(Specification<Company> companySpec, Pageable pageable);
    Optional<Company> findCompanyById(String id);
    Page<Company> search(String content, Pageable pageable);
}
