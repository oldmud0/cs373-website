package com.ethicalcompany.api.dao.company;

import com.ethicalcompany.api.domain.company.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("companyDao")
public interface PageableCompanyDao extends PagingAndSortingRepository<Company, String>, JpaSpecificationExecutor<Company> {
    @Query(value =
                "SELECT id, address, location, city_id, website, industry, summary, esg_scores, n_employees, name, market_cap, symbol," +
                "    regexp_replace(\n" +
                "        '...' || ts_headline(\n" +
                "            'Name: ' || name || ' • ' ||\n" +
                "            'Symbol: ' || symbol || ' • ' ||\n" +
                "            'Address: ' || address || ' • ' ||\n" +
                "            'Website: ' || website || ' • ' ||\n" +
                "            'Industry: ' || industry || ' • ' ||\n" +
                "            'Summary: ' || summary,\n" +
                "        to_tsquery(?1), 'MaxFragments=1') || '...',\n" +
                "        '\\.\\.\\.Name:', 'Name:') AS headline \n" +
                "FROM (\n" +
                "  SELECT \n" +
                "    *, \n" +
                "    to_tsvector(name) ||\n" +
                "    to_tsvector(address) || \n" +
                "    to_tsvector(website) ||\n" +
                "    to_tsvector(industry) ||\n" +
                "    to_tsvector(summary) ||\n" +
                "    to_tsvector(symbol)\n" +
                "    as document FROM companies\n" +
                "  GROUP BY id\n" +
                ") p_search\n" +
                "WHERE p_search.document @@ to_tsquery(?1)",
            countQuery =
                    "SELECT COUNT(*)\n" +
                    "FROM (\n" +
                    "  SELECT \n" +
                    "    *, \n" +
                    "    to_tsvector(name) ||\n" +
                    "    to_tsvector(address) || \n" +
                    "    to_tsvector(website) ||\n" +
                    "    to_tsvector(industry) ||\n" +
                    "    to_tsvector(summary) ||\n" +
                    "    to_tsvector(symbol)\n" +
                    "    as document FROM companies\n" +
                    "  GROUP BY id\n" +
                    ") p_search\n" +
                    "WHERE p_search.document @@ to_tsquery(?1)",
            nativeQuery = true)
    Page<Company> search(String content, Pageable pageable);
}
