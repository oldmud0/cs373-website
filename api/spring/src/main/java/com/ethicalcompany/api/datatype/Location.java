package com.ethicalcompany.api.datatype;

import java.io.Serializable;

public class Location implements Serializable {

    private Double latitude;
    private Double longitude;

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Location{" +
                ", latitude='" + latitude + '\'' +
                ", longitude=" + longitude + '\'' +
                '}';
    }
}