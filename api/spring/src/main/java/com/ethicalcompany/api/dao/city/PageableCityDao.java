package com.ethicalcompany.api.dao.city;

import com.ethicalcompany.api.domain.city.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("cityDao")
public interface PageableCityDao extends PagingAndSortingRepository<City, String>, JpaSpecificationExecutor<City> {
    @Query(value =
                "SELECT id, name, population, summary, metrics, overallscore, full_name, country, state, latlon, lation, location," +
                "    regexp_replace(\n" +
                "        '...' || ts_headline(\n" +
                "            'Full name: ' || full_name || ' • ' ||\n" +
                "            'Summary: ' || summary,\n" +
                "        to_tsquery(?1), 'MaxFragments=1') || '...',\n" +
                "        '\\.\\.\\.Name:', 'Name:') AS headline FROM (\n" +
                "    SELECT \n" +
                "        *, \n" +
                "        to_tsvector(name) || \n" +
                "        to_tsvector(summary) ||\n" +
                "        to_tsvector(full_name) ||\n" +
                "        to_tsvector(country) ||\n" +
                "        to_tsvector(state)\n" +
                "        as document FROM cities\n" +
                "        GROUP BY id) p_search " +
                "WHERE p_search.document @@ to_tsquery(?1)",
            countQuery = "SELECT count(*) FROM (\n" +
                    "    SELECT \n" +
                    "        *, \n" +
                    "        to_tsvector(name) || \n" +
                    "        to_tsvector(summary) ||\n" +
                    "        to_tsvector(full_name) ||\n" +
                    "        to_tsvector(country) ||\n" +
                    "        to_tsvector(state)\n" +
                    "        as document FROM cities\n" +
                    "GROUP BY id) p_search WHERE p_search.document @@ to_tsquery(?1)",
            nativeQuery = true)
    Page<City> search(String content, Pageable pageable);
}
