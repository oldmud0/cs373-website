package com.ethicalcompany.api.domain.city;

import com.ethicalcompany.api.domain.route.Route;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="cities")
public class City {
    @Id
    @Column(name="id")
    private String id;
    @Column(name="name")
    private String name;
    @Column(name="population")
    private Integer population;
    @Column(name="summary")
    private String summary;
    @Column(name="latlon")
    private String latlon;
    @Column(name="metrics")
    private String metrics;
    @Column(name="overallscore")
    private Double overallScore;
    @Column(name="full_name")
    private String fullName;
    @Column(name="country")
    private String country;
    @Column(name="state")
    private String state;
    @Column(name="headline")
    private String headline;

    @JsonIgnore
    @OneToMany(mappedBy = "city")
    private List<Route> routes;

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPopulation() {
        return population;
    }

    public String getSummary() {
        return summary;
    }

    public String getLatlon() {
        return latlon;
    }

    public String getMetrics() {
        return metrics.replace("score_out_of_10", "scoreOutOf10");
    }

    public Double getOverallScore() {
        return overallScore;
    }

    public String getFullName() {
        return fullName;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getHeadline() {
        return headline;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setLatlon(String latlon) {
        this.latlon = latlon;
    }

    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    public void setOverallScore(Double overallScore) {
        this.overallScore = overallScore;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    @Override
    public String toString() {
        return "City{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", population=" + population +
                ", summary='" + summary + '\'' +
                ", latlon='" + latlon + '\'' +
                ", metrics='" + metrics + '\'' +
                ", overallScore=" + overallScore +
                ", fullName='" + fullName + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", headline='" + headline + '\'' +
                ", routes=" + routes +
                '}';
    }
}
