package com.ethicalcompany.api.domain.route;

import com.ethicalcompany.api.domain.city.City;

import javax.persistence.*;

@Entity
@Table(name="routes")
public class Route {
    @Id
    @Column(name="id")
    private String id;
    @Column(name="onestop_id")
    private String onestopId;
    @Column(name="vehicle_type")
    private String vehicleType;
    @Column(name="stops")
    private String stops;
    @Column(name="bikes_allowed")
    private String bikesAllowed;
    @Column(name="wheelchair_accessible")
    private String wheelchairAccessible;
    @Column(name="geometry")
    private String geometry;
    @Column(name="operator_id")
    private String operatorId;

    @ManyToOne
    @JoinColumn(name="city_id")
    private City city;

    @Column(name="name")
    private String name;

    @Column(name="headline")
    private String headline;

    public String getId() {
        return id;
    }

    public String getOnestopId() {
        return onestopId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getStops() {
        return stops;
    }

    public String getBikesAllowed() {
        return bikesAllowed;
    }

    public String getWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public String getGeometry() {
        return geometry;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public City getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getHeadline() {
        return headline;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOnestopId(String onestopId) {
        this.onestopId = onestopId;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    public void setBikesAllowed(String bikesAllowed) {
        this.bikesAllowed = bikesAllowed;
    }

    public void setWheelchairAccessible(String wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    @Override
    public String toString() {
        return "Route{" +
                "id='" + id + '\'' +
                ", onestopId='" + onestopId + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", stops='" + stops + '\'' +
                ", bikesAllowed='" + bikesAllowed + '\'' +
                ", wheelchairAccessible='" + wheelchairAccessible + '\'' +
                ", geometry='" + geometry + '\'' +
                ", operatorId='" + operatorId + '\'' +
                ", city=" + city +
                ", name='" + name + '\'' +
                ", headline='" + headline + '\'' +
                '}';
    }
}
