package com.ethicalcompany.api.service.route;

import com.ethicalcompany.api.domain.route.Route;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

public interface RouteService {
    Page<Route> findAllRoute(
            String cityId,
            String name,
            List<String> vehicleType,
            List<String> bikesAllowed,
            List<String> wheelchairAccessible,
            Pageable pageable
    );

    Optional<Route> findRouteById(String id);
    Page<Route> search(String content, Pageable pageable);
}
