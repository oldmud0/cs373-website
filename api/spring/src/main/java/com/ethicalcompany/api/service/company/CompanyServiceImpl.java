package com.ethicalcompany.api.service.company;

import com.ethicalcompany.api.dao.company.PageableCompanyDao;
import com.ethicalcompany.api.domain.company.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService{
    @Autowired
    private PageableCompanyDao pageableCompanyDao;

    @Override
    public Page<Company> findAllCompany(Specification<Company> companySpec, Pageable pageable) {
        return pageableCompanyDao.findAll(companySpec, pageable);
    }

    @Override
    public Optional<Company> findCompanyById(String id) {
        return pageableCompanyDao.findById(id);
    }

    @Override
    public Page<Company> search(String content, Pageable pageable) {
        return pageableCompanyDao.search(content, pageable);
    }
}
