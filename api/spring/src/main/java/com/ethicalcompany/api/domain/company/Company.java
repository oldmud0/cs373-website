package com.ethicalcompany.api.domain.company;

import com.ethicalcompany.api.datatype.Location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="companies")
public class Company implements Serializable {
    @Id
    @Column(name="id")
    private String id;
    @Column(name="address")
    private String address;
    @Column(name="location")
    private String location;
    @Column(name="city_id")
    private String cityId;
    @Column(name="website")
    private String website;
    @Column(name="industry")
    private String industry;
    @Column(name="summary")
    private String summary;
    @Column(name="esg_scores")
    private String esgScores;
    @Column(name="n_employees")
    private String nEmployees;
    @Column(name="name")
    private String name;
    @Column(name="market_cap")
    private long marketCap;
    @Column(name="symbol")
    private String symbol;
    @Column(name="headline")
    private String headline;

    public String getnEmployees() {
        return nEmployees;
    }

    public String getHeadline() {
        return headline;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getLocation() {
        return location;
    }

    public String getCityId() {
        return cityId;
    }

    public String getWebsite() {
        return website;
    }

    public String getIndustry() {
        return industry;
    }

    public String getSummary() {
        return summary;
    }

    public String getEsgScores() {
        return esgScores;
    }

    public String getNEmployees() {
        return nEmployees;
    }

    public String getName() {
        return name;
    }

    public long getMarketCap() {
        return marketCap;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setCityId(String city_id) {
        this.cityId = city_id;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setEsgScores(String esgScores) {
        this.esgScores = esgScores;
    }

    public void setNEmployees(String n_employees) {
        this.nEmployees = n_employees;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMarketCap(long market_cap) {
        this.marketCap = market_cap;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setnEmployees(String nEmployees) {
        this.nEmployees = nEmployees;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id='" + id + '\'' +
                ", address='" + address + '\'' +
                ", location='" + location + '\'' +
                ", cityId='" + cityId + '\'' +
                ", website='" + website + '\'' +
                ", industry='" + industry + '\'' +
                ", summary='" + summary + '\'' +
                ", esgScores='" + esgScores + '\'' +
                ", nEmployees='" + nEmployees + '\'' +
                ", name='" + name + '\'' +
                ", marketCap=" + marketCap +
                ", symbol='" + symbol + '\'' +
                ", headline='" + headline + '\'' +
                '}';
    }
}
