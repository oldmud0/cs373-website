package com.ethicalcompany.api.service.route;

import com.ethicalcompany.api.dao.route.PageableRouteDao;
import com.ethicalcompany.api.domain.route.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("routeService")
public class RouteServiceImpl implements RouteService{
    @Autowired
    private PageableRouteDao pageableRouteDao;

    @Override
    public Page<Route> findAllRoute(
            @Param("cityId") String cityId,
            @Param("name") String name,
            @Param("vehicleType") List<String> vehicleType,
            @Param("bikesAllowed") List<String> bikesAllowed,
            @Param("wheelchairAccessible") List<String> wheelchairAccessible,
            Pageable pageable
    ) {
        return pageableRouteDao.findAll(cityId, name, vehicleType, bikesAllowed, wheelchairAccessible, pageable);
    }

    @Override
    public Optional<Route> findRouteById(String id) {
        return pageableRouteDao.findById(id);
    }

    @Override
    public Page<Route> search(String content, Pageable pageable) {
        return pageableRouteDao.search(content, pageable);
    }

}
