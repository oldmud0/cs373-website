package com.ethicalcompany.api.controller.company;

import java.util.Optional;

import com.ethicalcompany.api.domain.company.Company;
import com.ethicalcompany.api.domain.company.CompanyEndpointInfo;
import com.ethicalcompany.api.service.company.CompanyService;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.In;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

@And({
        @Spec(path = "cityId", spec = Equal.class),
        @Spec(path = "name", spec = LikeIgnoreCase.class),
        @Spec(path = "industry", spec = In.class)
})
interface CompanySpec extends Specification<Company> {}

@RestController
@CrossOrigin
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @RequestMapping("/v1/company/list")
    public Page<Company> getCompanyList(
            CompanySpec companySpec,
            @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        return companyService.findAllCompany(companySpec, pageable);
    }

    @RequestMapping("/v1/company/detail")
    public Optional<Company> getCompanyDetail(
            @RequestParam(value="id", defaultValue = "1") String id) {
        return companyService.findCompanyById(id);
    }

    @RequestMapping("/v1/company/info")
    public CompanyEndpointInfo getInfo() {
        return new CompanyEndpointInfo();
    }

    @RequestMapping("/v1/company/search")
    public Page<Company> searchCompany(
            @RequestParam(value = "content", defaultValue = "App") String content,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "15") Integer size) {
        Pageable pageable = new PageRequest(page, size);
        return companyService.search(content, pageable);
    }
}
