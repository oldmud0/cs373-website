package com.ethicalcompany.api.service.city;

import com.ethicalcompany.api.domain.city.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;

public interface CityService {
    Page<City> findAllCity(Specification<City> spec, Pageable pageable);
    Optional<City> findCityById(String id);
    Page<City> search(String content, Pageable pageable);
}
