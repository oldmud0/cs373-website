package com.ethicalcompany.api.dao.route;

import com.ethicalcompany.api.domain.route.Route;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("routeDao")
public interface PageableRouteDao extends PagingAndSortingRepository<Route, String>, JpaSpecificationExecutor<Route> {
    @Query(value =
            "SELECT id, onestop_id, vehicle_type, stops, bikes_allowed, wheelchair_accessible, geometry, operator_id, city_id, name, city_name, operator_name, regexp_replace(\n" +
                    "        '...' || ts_headline(\n" +
                    "            'Name: ' || name || ' • ' ||\n" +
                    "            'Stops: ' || stops || ' • ' ||\n" +
                    "            'Vehicle: ' || vehicle_type || ' • ' ||\n" +
                    "            'City: ' || city_name || ' • ' ||\n" +
                    "            'Operator: ' || operator_name || ' • ',\n" +
                    "        to_tsquery(?1), 'MaxFragments=1') || '...',\n" +
                    "        '\\.\\.\\.Name:', 'Name:') AS headline FROM (\n" +
                    "    SELECT \n" +
                    "        routes.id, \n" +
                    "        routes.onestop_id, \n" +
                    "        routes.vehicle_type, \n" +
                    "        routes.stops, \n" +
                    "        routes.bikes_allowed,\n" +
                    "        routes.wheelchair_accessible, \n" +
                    "        routes.geometry,\n" +
                    "        routes.operator_id,\n" +
                    "        routes.city_id,\n" +
                    "        routes.name,\n" +
                    "        cities.name as city_name,\n" +
                    "        operators.name as operator_name,\n" +
                    "        to_tsvector(routes.name) ||\n" +
                    "        to_tsvector(routes.stops) ||\n" +
                    "        to_tsvector(routes.vehicle_type) || \n" +
                    "        to_tsvector(cities.name) ||\n" +
                    "        to_tsvector(operators.name)\n" +
                    "        as document FROM routes \n" +
                    "            JOIN cities ON routes.city_id = cities.id\n" +
                    "            JOIN operators ON routes.operator_id = operators.id\n" +
                    "GROUP BY routes.id, cities.id, operators.id) p_search WHERE p_search.document @@ to_tsquery(?1)",
            countQuery = "SELECT count(*) FROM (\n" +
                    "    SELECT \n" +
                    "        routes.id, \n" +
                    "        routes.onestop_id, \n" +
                    "        routes.vehicle_type, \n" +
                    "        routes.stops, \n" +
                    "        routes.bikes_allowed,\n" +
                    "        routes.wheelchair_accessible, \n" +
                    "        routes.geometry,\n" +
                    "        routes.operator_id,\n" +
                    "        routes.city_id,\n" +
                    "        routes.name,\n" +
                    "        to_tsvector(routes.vehicle_type) || \n" +
                    "        to_tsvector(routes.stops) ||\n" +
                    "        to_tsvector(cities.name) ||\n" +
                    "        to_tsvector(routes.name) ||\n" +
                    "        to_tsvector(operators.name)\n" +
                    "        as document FROM routes \n" +
                    "            JOIN cities ON routes.city_id = cities.id\n" +
                    "            JOIN operators ON routes.operator_id = operators.id\n" +
                    "GROUP BY routes.id, cities.id, operators.id) p_search WHERE p_search.document @@ to_tsquery(?1)",
            nativeQuery = true)
    Page<Route> search(String content, Pageable pageable);

    @Query(
            value = "select r from Route r where " +
                    "(r.city is not null) and " +
                    "(:cityId is null or r.city.id = :cityId) and" +
                    "(:name is null or r.name = :name) and" +
                    "((:vehicleType) is null or r.vehicleType in :vehicleType) and" +
                    "((:bikesAllowed) is null or r.bikesAllowed in :bikesAllowed) and" +
                    "((:wheelchairAccessible) is null or r.wheelchairAccessible in :wheelchairAccessible)"
    )
    Page<Route> findAll(
            @Param("cityId") String cityId,
            @Param("name") String name,
            @Param("vehicleType") List<String> vehicleType,
            @Param("bikesAllowed") List<String> bikesAllowed,
            @Param("wheelchairAccessible") List<String> wheelchairAccessible,
            Pageable pageable
    );
}