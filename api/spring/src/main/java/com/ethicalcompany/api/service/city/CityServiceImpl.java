package com.ethicalcompany.api.service.city;

import com.ethicalcompany.api.dao.city.PageableCityDao;
import com.ethicalcompany.api.domain.city.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("cityService")
public class CityServiceImpl implements CityService {
    @Autowired
    private PageableCityDao pageableCityDao;

    @Override
    public Page<City> findAllCity(Specification<City> spec, Pageable pageable) {
        return pageableCityDao.findAll(spec, pageable);
    }

    @Override
    public Optional<City> findCityById(String id) {
        return pageableCityDao.findById(id);
    }

    @Override
    public Page<City> search(String content, Pageable pageable) {
        return pageableCityDao.search(content, pageable);
    }
}
