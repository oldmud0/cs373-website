#!/bin/bash

# Source: https://almcc.me/blog/2017/05/04/removing-older-versions-on-google-app-engine/
VERSIONS=$(gcloud app versions list --service $1 --sort-by '~version' --format 'value(version.id)')
COUNT=0
echo "Keeping the $2 latest versions of the $1 service..."
for VERSION in $VERSIONS
do
    ((COUNT++))
    if [ $COUNT -gt $2 ]
    then
      echo "Deleting version $VERSION"
      gcloud app versions delete $VERSION --service $1 -q
    else
      echo "Keeping version $VERSION"
    fi
done
