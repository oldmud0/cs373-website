module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    'react/jsx-filename-extension': [2, { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    'react/jsx-one-expression-per-line': 0,
    'react/state-in-constructor': 0,
    'react/prop-types': 0, // typescript takes care of this
    'react/prefer-stateless-function': 'warn',
    'import/no-extraneous-dependencies': [2, { devDependencies: ['**/*test.tsx', '**/*test.ts', '**/setupTests.ts'] }],
    '@typescript-eslint/indent': [2, 2],
    'no-undef': 0, // safe since TS won't compile with undefined variables, need to to work around some parser bug
    'no-unused-vars': 0,
  },
  settings: {
    "import/resolver": {
      "node": {
        "extensions": [".js", ".jsx", ".ts", ".tsx", ".d.ts"]
      }
    }
  }
};
