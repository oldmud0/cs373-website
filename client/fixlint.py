import subprocess
import os, sys
from pathlib import Path
import pandas as pd
import re
import urllib
import json
from datetime import datetime, timedelta

def parse_info_file(file):
    all_lines = []
    for line in file:
        line = re.sub('\t',"    ",line)
        line = re.sub("\n","",line)
        all_lines.append(line)
    return "\n".join(all_lines)

def parse_dir(directory):
    subdirs = [x for x in directory.iterdir() if x.is_dir()]

    ts_files = [x for x in directory.glob('**/*.ts')]
    tsx_files = [x for x in directory.glob('**/*.tsx')]

    for anki_info_path in ts_files:
        with anki_info_path.open() as anki_info_file:
            result = parse_info_file(anki_info_file)
        with anki_info_path.open("w+") as anki_info_file:
            anki_info_file.write(result)
    
    for anki_info_path in tsx_files:
        with anki_info_path.open() as anki_info_file:
            result = parse_info_file(anki_info_file)
        with anki_info_path.open("w+") as anki_info_file:
            anki_info_file.write(result)

cur_dir = Path.cwd() / "src"
parse_dir(cur_dir)