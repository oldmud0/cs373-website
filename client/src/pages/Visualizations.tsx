import React from 'react';
import Container from 'react-bootstrap/Container';
import Company from '../types/Company';
import { getPage } from '../utils/request';
import { CompanyListUrl, CityListUrl, Loader } from '../components/Constants/Constants';
import { companyParser, cityParser } from '../utils/parsers';
import CompaniesByIndustry from '../components/Visualizations/CompaniesByIndustry';
import City from '../types/City';
import CityVisualization from '../components/Visualizations/CityVisualization';
import EsgScoreVisualization from '../components/Visualizations/EsgScoreVisualization';
import PollutionVsHealthIndex from '../components/Visualizations/customer/ClimateVsPollution';
import SalaryVsProjection from '../components/Visualizations/customer/SalaryVsProjection';
import MinorityReprMap from '../components/Visualizations/customer/MinorityReprMap';

type VisualizationPageState = {
  allCompanies: Company[];
  allCities: City[];
}

class VisualizationPage extends React.Component<{}, VisualizationPageState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      allCompanies: [],
      allCities: [],
    };
  }

  async componentDidMount() {
    try {
      const [companies, cities] = await Promise.all([
        this.getCompanies(),
        this.getCities(),
      ]);

      this.setState({
        allCompanies: companies.content,
        allCities: cities.content,
      });
    } catch (e) {
      console.log(e);
    }
  }

  private getCompanies = () => (
    getPage(CompanyListUrl, { page: 0, size: 500 }, companyParser)
  )

  private getCities = () => (
    getPage(CityListUrl, {
      page: 0,
      size: 400,
      filters: {
        country: ['United States'],
      },
    }, cityParser)
  )

  render() {
    const { allCompanies, allCities } = this.state;

    // TODO: use a real loader
    return (
      <Container>
        <h1>Visualizations</h1>
        {allCities.length
          ? <CityVisualization height={750} width={1200} cities={allCities} />
          : <Loader />}
        {allCompanies.length
          ? <CompaniesByIndustry height={450} width={700} companies={allCompanies} />
          : <Loader />}
        {allCompanies.length
          ? <EsgScoreVisualization height={600} width={1200} companies={allCompanies} />
          : <Loader />}
        <h1>Customer visualizations</h1>
        <PollutionVsHealthIndex height={600} width={600} />
        <SalaryVsProjection height={600} width={600} />
        <MinorityReprMap height={750} width={1200} />
      </Container>
    );
  }
}

export default VisualizationPage;
