import React from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import DirectoryHeader from '../../components/Directory/DirectoryHeader';
import City from '../../types/City';
import Company from '../../types/Company';
import TransitRoute from '../../types/TransitRoute';
import FilterInstance from '../../types/FilterInstance';
import SortHeaderEntry from '../../types/SortHeaderEntry';
import EntryValues from '../../types/EntryValues';
import DirectoryPagination from '../../components/Directory/DirectoryPagination';
import Entry from '../../components/Directory/Entry';
import { DirectoryTableWrapper, FixedTableHeader } from '../../components/Constants/Constants';
import SortableTableHeader, { StyledTh, SortOrder } from '../../components/Sorting/SortableTableHeader';
import { getPage, QueryParams } from '../../utils/request';
import Filter, { FilterOption } from '../../components/Filtering';

type DirectoryProps = {
  pageSize: number;
  filterInstance: FilterInstance[];
  defaultSort: string;
  defaultSortOrder: number;
  parser: (response: any) => any;
  searchUrl: string;
  listUrl: string;
  header: string;
  sortHeader: SortHeaderEntry[];
  formatter: (entry: any) => EntryValues;
}

type DirectoryState = {
  totalPages: number;
  entries: any;
  sortBy: string;
  sortOrder: number;
  filterOptions: {
    [index: string]: string[];
  }
  search: string;
}

class Directory extends React.Component
  <DirectoryProps, DirectoryState> {
  filterComponent: any = React.createRef();

  searchComponent: any = React.createRef();

  constructor(props: DirectoryProps) {
    super(props);

    const filterOptions: { [index: string] : string[] } = props.filterInstance
      .reduce((acc: { [k: string]: string[] }, cur) => {
        acc[cur.optionsKey] = [];
        return acc;
      }, {});

    this.state = {
      totalPages: 0,
      entries: [],
      sortBy: props.defaultSort,
      sortOrder: props.defaultSortOrder,
      search: '',
      filterOptions,
    };
  }

  componentDidMount() {
    this.switchPage();
  }

  switchPage = (params?: QueryParams) => {
    const {
      pageSize, parser, searchUrl, listUrl,
    } = this.props;
    const {
      sortBy, sortOrder, filterOptions, search,
    } = this.state;

    const requestParams = {
      size: pageSize,
      page: 0,
      sortBy,
      sortOrder,
      filters: filterOptions,
      search,
      ...params,
    };

    const url = search ? searchUrl : listUrl;

    return getPage(url, requestParams, parser)
      .then((page) => {
        this.setState({
          totalPages: page.totalPages,
          entries: page.content,
        });
      })
      .catch(() => console.log);
  }

  changeSort = (field: string) => async () => {
    const { sortBy, sortOrder } = this.state;
    const { current } = this.searchComponent;

    current.clearSearch();

    this.setState({
      sortBy: field,
      sortOrder: sortBy === field ? -sortOrder : SortOrder.DESC,
      search: '',
    }, async () => {
      await this.switchPage();
    });
  }

  onFilterChange = (filterName: string, option: string) => {
    const { filterOptions } = this.state;
    const { current } = this.searchComponent;

    const newFilter = filterOptions[filterName].includes(option)
      ? filterOptions[filterName].filter((opt) => opt !== option)
      : [...filterOptions[filterName], option];

    current.clearSearch();

    this.setState({
      filterOptions: {
        ...filterOptions,
        [filterName]: newFilter,
      },
      search: '',
    }, async () => {
      await this.switchPage();
    });
  }

  onSearch = (value: string) => {
    if (value) {
      const { current } = this.filterComponent;
      const { filterInstance } = this.props;
      current.resetFilter();
      this.setState({
        sortBy: 'name',
        sortOrder: SortOrder.ASC,
        search: value,
        filterOptions: filterInstance
          .reduce((acc: { [k: string]: string[] }, cur) => {
            acc[cur.optionsKey] = [];
            return acc;
          }, {}),
      }, async () => {
        await this.switchPage();
      });
    }
  }

  render() {
    const {
      totalPages, entries, sortBy, sortOrder, search,
    } = this.state;
    const {
      header, filterInstance, sortHeader, formatter,
    } = this.props;

    return (
      <Container>
        <Row className="m-3">
          <Col>
            <DirectoryHeader
              header={header}
              callback={this.onSearch}
              ref={this.searchComponent}
            />
          </Col>
        </Row>
        <Row className="m-3">
          <Col>
            <Filter>
              {filterInstance.map((opt) => (
                <FilterOption
                  key={opt.optionsKey}
                  options={opt.options}
                  optionsKey={opt.optionsKey}
                  title={opt.title}
                  onChange={this.onFilterChange}
                  ref={this.filterComponent}
                />
              ))}
            </Filter>
            <DirectoryTableWrapper className="border rounded-lg overflow-auto bg-white">
              <Table className="table-hover">
                <FixedTableHeader>
                  <tr>
                    {sortHeader.map((curr) => {
                      if (curr.value) {
                        return (
                          <SortableTableHeader
                            active={sortBy === curr.value}
                            sortOrder={sortOrder}
                            onClick={this.changeSort(curr.value)}
                          >
                            {curr.label}
                          </SortableTableHeader>
                        );
                      }
                      return (
                        <StyledTh>{curr.label}</StyledTh>
                      );
                    })}
                  </tr>
                </FixedTableHeader>
                <tbody>
                  {entries.map((entry: any) => (
                    <Entry
                      entryValues={formatter(entry)}
                      searchQuery={search}
                    />
                  ))}
                </tbody>
              </Table>
            </DirectoryTableWrapper>
            <DirectoryPagination totalPages={totalPages} callback={this.switchPage} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Directory;
