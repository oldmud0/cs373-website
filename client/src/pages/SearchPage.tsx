import React from 'react';
import styled from 'styled-components';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Nav from 'react-bootstrap/Nav';
import { CitySearchUrl, CompanySearchUrl, TransitRouteSearchUrl } from '../components/Constants/Constants';
import { getPage } from '../utils/request';
import { cityParser, transitRouteParser, companyParser } from '../utils/parsers';
import TransitRoute from '../types/TransitRoute';
import City from '../types/City';
import Company from '../types/Company';
import CitySearchResult from '../components/Search/CitySearchResult';
import SearchFilters from '../components/Search/SearchFilters';
import CompanySearchResult from '../components/Search/CompanySearchResult';
import RouteSearchResult from '../components/Search/TransitRouteSearchResult';

const InputWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 25px;
`;

type SearchPageState = {
  query: string;
  cityResults: City[];
  companyResults: Company[];
  routeResults: TransitRoute[];
  resultFilter: string;
  submitted: boolean;
};

class SearchPage extends React.Component<{}, SearchPageState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      query: '',
      cityResults: [],
      companyResults: [],
      routeResults: [],
      resultFilter: 'all',
      submitted: false,
    };
  }

  shouldComponentUpdate(nextProps: {}, nextState: SearchPageState) {
    return nextState.submitted;
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { submitted } = this.state;
    this.setState({
      query: e.currentTarget.value,
      submitted: false,
    });
  }

  handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') this.handleSearch();
  }

  handleSearch = async () => {
    const { query } = this.state;

    if (!query) return;

    const requestParams = {
      size: 15,
      page: 0,
      search: query,
    };

    const [cityResults, companyResults, routeResults]: any = await Promise.all([
      { url: CitySearchUrl, parser: cityParser },
      { url: CompanySearchUrl, parser: companyParser },
      { url: TransitRouteSearchUrl, parser: transitRouteParser },
    ].map(({ url, parser }) => getPage<unknown>(url, requestParams, parser)));

    this.setState({
      cityResults: cityResults.content,
      companyResults: companyResults.content,
      routeResults: routeResults.content,
      submitted: true,
    });
  };

  handleNavClick = (eventKey: string, e: React.SyntheticEvent<any, Event>) => {
    this.setState({
      resultFilter: eventKey,
    });
  }

  render() {
    const {
      query, cityResults, companyResults, routeResults, resultFilter, submitted,
    } = this.state;

    const noResults = query
      && submitted
      && ((cityResults.length + companyResults.length + routeResults.length) === 0);

    return (
      <>
        <InputWrapper>
          <InputGroup>
            <FormControl
              type="text"
              placeholder="Search for anything..."
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
            />
          </InputGroup>
          <InputGroup.Append>
            <Button onClick={this.handleSearch}>Search</Button>
          </InputGroup.Append>
        </InputWrapper>
        <SearchFilters active={resultFilter} onSelect={this.handleNavClick} />
        {noResults && (
          <h3>No results</h3>
        )}
        {['cities', 'all'].includes(resultFilter) && cityResults.map((city) => (
          <CitySearchResult key={city.id} city={city} query={query} />
        ))}
        {['companies', 'all'].includes(resultFilter) && companyResults.map((company) => (
          <CompanySearchResult key={company.id} company={company} query={query} />
        ))}
        {['routes', 'all'].includes(resultFilter) && routeResults.map((route) => (
          <RouteSearchResult key={route.id} route={route} query={query} />
        ))}
      </>
    );
  }
}

export default SearchPage;
