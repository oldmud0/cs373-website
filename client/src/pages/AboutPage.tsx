import React from 'react';
import styled from 'styled-components';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import willJpg from '../static/members/will.jpg';
import bennettJpg from '../static/members/bennett.jpg';
import michaelJpg from '../static/members/michael.jpg';
import milesJpg from '../static/members/miles.jpg';
import spencerJpg from '../static/members/spencer.jpg';

import testCounts from '../static/tests.json';

import { request } from '../utils/request';

type ProfileStats = {
  issues: {
    all: number,
    opened: number,
    closed: number
  },
  commits: number,
  tests: number,
}

type ProfileCardProps = {
  name: string,
  imgSrc: string,
  devRole: string,
  bio: string,
  stats: ProfileStats,
}

const ProfileCardImage = styled.div`
  max-height: 300px;
  overflow: hidden;

  img {
    width: 100%;
    height: auto;
  }
`;

const ProfileCard = ({
  name, devRole, imgSrc, bio, stats,
}: ProfileCardProps) => (
  <Card style={{ marginTop: '40px' }}>
    <ProfileCardImage>
      <Card.Img variant="top" src={imgSrc} />
    </ProfileCardImage>
    <Card.Body style={{ minHeight: '150px' }}>
      <Card.Title>{`${name} (${devRole})`}</Card.Title>
      <Card.Text>
        {bio}
      </Card.Text>
    </Card.Body>
    <Card.Footer className="text-muted">
      {(stats && stats.issues) ? stats.issues.all : '...'}
      {' '}
      issues,
      {' '}
      {(stats && stats.commits) ? stats.commits : '...'}
      {' '}
      commits,
      {' '}
      {(stats && stats.tests !== undefined) ? stats.tests : '...'}
      {' '}
      unit tests
    </Card.Footer>
  </Card>
);

type AboutPageState = {
  stats: {
    [k: string]: ProfileStats
  },
  totalCommits: number,
  totalIssues: number,
  totalTests: number,
}

const AboutContainer = styled.div`
  width: 70%;
  margin: 100px auto 0px auto;
`;

const AboutParagraph = styled.p`
  margin-top: 40px;
  font-size: 20px;
`;

class AboutPage extends React.Component<{}, AboutPageState> {
  constructor(props: any) {
    super(props);
    this.state = {
      stats: {},
      totalCommits: 0,
      totalIssues: 0,
      totalTests: 0,
    };
  }

  async componentDidMount() {
    const PROJECT_ID = 14416778;
    const members: { [k: string]: string[] } = {
      williamli1: ['Williamli3456'],
      oldmud0: ['oldmud0'],
      SpencerSharp: ['Spencer Sharp'],
      d8tltanc: ['d8tltanc'],
      'mi-yu1': ['mi-yu'],
    };

    const memberUsernames = Object.keys(members);

    const issuesPerMember = (await Promise.all(memberUsernames.map(async (user) => {
      const issueStats = await request(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues_statistics?assignee_username=${user}&scope=all`);
      return {
        user,
        issues: issueStats.statistics.counts,
      };
    }))).reduce((acc: { [k:string]: any }, stat) => {
      acc[stat.user] = stat.issues;
      return acc;
    }, {});

    const commitStats = await request(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/contributors`);
    const totalCommits = commitStats
      .reduce((commits: number, stat: any) => commits + stat.commits, 0);

    const commitsPerMember = memberUsernames.reduce((acc: { [k: string]: any }, user) => {
      // There might be the same user with different emails - these are counted separately in GitLab
      const userStats = commitStats
        .filter((stat: any) => members[user].includes(stat.name))
        .reduce((commits: number, stat: any) => commits + stat.commits, 0);
      acc[user] = userStats;
      return acc;
    }, {});

    const totalIssues = await request(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues_statistics`);
    const totalTests = Object.values(testCounts).reduce((acc, cur) => acc + cur, 0);

    const perMemberStats = memberUsernames.reduce((acc: { [k: string]: any}, user) => {
      acc[user] = {
        issues: issuesPerMember[user],
        commits: commitsPerMember[user],
        tests: (testCounts as {[user: string]: number})[user],
      };

      return acc;
    }, {});

    this.setState({
      stats: {
        ...perMemberStats,
      },
      totalCommits,
      totalIssues: totalIssues.statistics.counts.all,
      totalTests,
    });
  }

  render() {
    const {
      stats,
      totalCommits,
      totalIssues,
      totalTests,
    } = this.state;

    return (
      <>
        <AboutContainer>
          <Row className="m-3">
            <Col>
              <h1>About Us</h1>
              <AboutParagraph>
                Ethical Employers focuses on the environmental aspect of working for different
                companies. Different cities, locations, and companies have different levels of
                impact on the environment, and Ethical Employers is here to help potential
                employees choose positions with the most positive environmental impacts. We
                hope to have this encompass everything from the transportation method and
                route that you choose to get to work to the recycling policy involved in the
                city that the company is located in.
              </AboutParagraph>
              <AboutParagraph>
                Why should <em>you</em> use Ethical Employers? It&#39;s a one stop shop for
                exploring ethical ramifications of your employer. You can feel comfortable and
                confident about your employer&#39;s ethical actions and know that you&#39;re
                making a positive difference in the world through your work. Even if you
                don&#39;t have a job at the moment, you can find cities with a track record
                of ethical, environmentally responsible practices to model your own
                environmentally conscious decisions and to remain politically informed
                about local government environmental policy around the nation.
              </AboutParagraph>
            </Col>
          </Row>
          <Row className="m-3">
            <Col>
              <h1>Members</h1>
            </Col>
          </Row>
        </AboutContainer>
        <Container style={{ marginTop: '25px' }}>
          <Row className="m-3">
            <Col md={4}>
              <ProfileCard
                name='William "Will" Li'
                devRole="frontend"
                imgSrc={willJpg}
                bio="Will seems to keep an air of mystery about him…"
                stats={stats.williamli1}
              />
            </Col>
            <Col md={4}>
              <ProfileCard
                name="Bennett Ramirez"
                devRole="DevOps"
                imgSrc={bennettJpg}
                bio="Considered legendary in some circles – just not any that he knows of."
                stats={stats.oldmud0}
              />
            </Col>
            <Col md={4}>
              <ProfileCard
                name="Spencer Sharp"
                devRole="logistics"
                imgSrc={spencerJpg}
                bio="A seasoned developer strongly oriented in databases and the backend."
                stats={stats.SpencerSharp}
              />
            </Col>
          </Row>
          <Row className="justify-content-around m-3 mt-3">
            <Col md={4}>
              <ProfileCard
                name='Cheng "Miles" Tan'
                devRole="backend"
                imgSrc={milesJpg}
                bio="An ICPC wanna-be, ready to graduate and enter the industry."
                stats={stats.d8tltanc}
              />
            </Col>
            <Col md={4}>
              <ProfileCard
                name="Michael Yu"
                devRole="scraping"
                imgSrc={michaelJpg}
                bio='"I used to do a lot of JavaScript, but not anymore hopefully."'
                stats={stats['mi-yu1']}
              />
            </Col>
          </Row>
        </Container>
        <AboutContainer>
          <Row className="m-3">
            <Col>
              <h1>Repository stats</h1>
            </Col>
          </Row>
          <Row style={{ margin: '50px 0' }}>
            <Col md={4}>
              <h1 className="text-center">
                {totalCommits}
                {' '}
                commits
              </h1>
            </Col>
            <Col md={4}>
              <h1 className="text-center">
                {totalIssues}
                {' '}
                issues
              </h1>
            </Col>
            <Col md={4}>
              <h1 className="text-center">
                {totalTests}
                {' '}
                unit tests
              </h1>
            </Col>
          </Row>
          <Row className="m-3">
            <Col>
              <h1>Data sources</h1>
            </Col>
          </Row>
          <Row className="m-3">
            <Col>
              <p>
                Data collected is from <a href="https://rapidapi.com/apidojo/api/yahoo-finance1">Yahoo Finance</a>,
                the <a href="https://developers.google.com/maps/documentation/geocoding/start">Google Maps geocoder </a>,
                and the <a href="https://developers.teleport.org/api/">Teleport API</a>.
              </p>
              <p>
                All were scraped using their respective JSON APIs;
                see the <a href="https://gitlab.com/oldmud0/cs373-website/tree/master/scrapers"><code>scrapers</code></a> folder
                in the GitLab repository for the Python scripts used.
              </p>
            </Col>
          </Row>
          <Row className="m-3">
            <Col>
              <h1>Tools</h1>
            </Col>
          </Row>
          <Row className="m-3">
            <Col>
              <p>
                Tools used for scraping include the Python
                <a
                  href="https://requests.kennethreitz.org/en/master/"
                >
                  <code>requests</code>
                </a>
                library (for programmatic JSON requests) and <a href="https://getpostman.com">Postman</a> (for interactive API
                interactions). No optional tools have currently been used yet.
              </p>
              <h3><a href="https://gitlab.com/oldmud0/cs373-website">GitLab repository</a></h3>
              <h3><a href="https://documenter.getpostman.com/view/8557468/SW7dW71M">Postman API</a></h3>
            </Col>
          </Row>
        </AboutContainer>
      </>
    );
  }
}

export default AboutPage;
