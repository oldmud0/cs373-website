import React from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import { RouteComponentProps } from 'react-router-dom';
import {
  CompanyInstanceUrl, environmentScoreDescription, governanceScoreDescription,
  socialScoreDescription, esgScoreDescription,
} from '../../components/Constants/Constants';
import CompanySummary from '../../components/Instance/Company/CompanySummary';
import CompanyInfo from '../../components/Instance/Company/CompanyInfo';
import CompanyESGFactors from '../../components/Instance/Company/CompanyESGFactors';
import CompanyESGControversyPanel from '../../components/Instance/Company/CompanyESGControversyPanel';
import CompanyESGScorePanel from '../../components/Instance/Company/CompanyESGScorePanel';
import { instanceRequest } from '../../utils/request';
import { companyParser } from '../../utils/parsers';
import Company from '../../types/Company';

class CompanyInstance extends React.Component
  <{} & RouteComponentProps, { id: string, company: Company }> {
  state = {
    id: '',
    company: {} as Company,
  }

  componentDidMount() {
    const { match } = this.props;
    const params = match.params as any;

    instanceRequest(CompanyInstanceUrl, params.id, companyParser)
      .then((company) => {
        this.setState({
          id: params.id,
          company,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { id, company } = this.state;
    const { esgScores } = company;

    if (id === '') {
      return <div />;
    }

    return (
      <Container>
        <Row className="m-3">
          <Col><h1>{ company.name }</h1></Col>
        </Row>
        <Row className="m-3">
          <Col>
            <CompanyInfo company={company} />
          </Col>
        </Row>
        <Row className="m-3">
          <Col>
            <CompanySummary summary={company.summary} />
          </Col>
        </Row>
        <Row className="m-3">
          <Col>
            <CompanyESGFactors esgScores={esgScores} />
          </Col>
          <Col>
            <CompanyESGControversyPanel controversy={esgScores.controversyDetail} />
          </Col>
        </Row>
        <Row className="m-3">
          <Col>
            <CompanyESGScorePanel
              title="Environment Score"
              description={environmentScoreDescription}
              esgScoreDetail={esgScores.environmentDetail}
            />
          </Col>
          <Col>
            <CompanyESGScorePanel
              title="Governance Score"
              description={governanceScoreDescription}
              esgScoreDetail={esgScores.governanceDetail}
            />
          </Col>
        </Row>
        <Row className="m-3">
          <Col>
            <CompanyESGScorePanel
              title="Social Score"
              description={socialScoreDescription}
              esgScoreDetail={esgScores.socialDetail}
            />
          </Col>
          <Col>
            <CompanyESGScorePanel
              title="Total ESG Score"
              description={esgScoreDescription}
              esgScoreDetail={esgScores.esgDetail}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default CompanyInstance;
