import React from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import { RouteComponentProps } from 'react-router-dom';
import { CityInstanceUrl, LinkedRow } from '../../components/Constants/Constants';
import CityBasicInfo from '../../components/Instance/City/CityBasicInfo';
import CitySummary from '../../components/Instance/City/CitySummary';
import CityMetrics from '../../components/Instance/City/CityMetrics';
import CityCompanies from '../../components/Instance/City/CityCompanies';
import CityTransitRoutes from '../../components/Instance/City/CityTransitRoutes';
import City, { Coordinate, CityMetric } from '../../types/City';
import { instanceRequest } from '../../utils/request';
import { cityParser } from '../../utils/parsers';

const DIRECTORY_PAGE_SIZE = 3;

class CityInstance extends React.Component<{} & RouteComponentProps, { id: string, city: City }> {
  state = {
    id: '',
    city: {} as City,
  }

  componentDidMount() {
    const { match } = this.props;
    const params = match.params as any;

    instanceRequest(CityInstanceUrl, params.id, cityParser)
      .then((city) => {
        this.setState({
          id: params.id,
          city,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { id, city } = this.state;

    if (id === '') {
      return <div />;
    }

    return (
      <Container>
        <Row className="m-3">
          <Col><h1>{city.name}</h1></Col>
        </Row>
        <Row className="m-3">
          <Col>
            <CityBasicInfo city={city} />
            <CitySummary summary={city.summary} />
          </Col>
          <Col>
            <CityMetrics metrics={city.metrics} />
          </Col>
        </Row>
        <Row className="m-3">
          <Col>
            <CityCompanies pageSize={DIRECTORY_PAGE_SIZE} id={id} />
          </Col>
          <Col>
            <CityTransitRoutes pageSize={DIRECTORY_PAGE_SIZE} id={id} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default CityInstance;
