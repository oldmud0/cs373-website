import React from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import { Link, RouteComponentProps } from 'react-router-dom';
import {
  LinkedRow, TransitRouteInstanceUrl, CityInstanceUrl, CompanyListUrl,
} from '../../components/Constants/Constants';
import TransitRoute from '../../types/TransitRoute';
import City from '../../types/City';
import { instanceRequest, getPage } from '../../utils/request';
import { cityParser, transitRouteParser } from '../../utils/parsers';
import Company from '../../types/Company';

type TransitRouteInstanceState = {
  id: string;
  route: TransitRoute;
  city: City;
  nearbyCompanies: Company[];
}

const formatData = (data: string) => data.split('_')
  .map((word) => `${word.charAt(0).toUpperCase()}${word.substr(1)}`)
  .join(' ');

class TransitRouteInstance extends React.Component<RouteComponentProps, TransitRouteInstanceState> {
  constructor(props: RouteComponentProps) {
    super(props);

    this.state = {
      id: '',
      route: {} as TransitRoute,
      city: {} as City,
      nearbyCompanies: [] as Company[],
    };
  }

  async componentDidMount() {
    const { match } = this.props;
    const params = match.params as any;

    try {
      const route = await instanceRequest(TransitRouteInstanceUrl, params.id, transitRouteParser);
      if (route.city && route.city.id) {
        const city = await instanceRequest(CityInstanceUrl, route.city.id, cityParser);
        this.setState({
          id: params.id,
          route,
          city,
        });

        const nearbyCompanies = await this.fetchNearbyCompanies(route.city.id);
        this.setState({
          nearbyCompanies,
        });
      } else {
        this.setState({
          id: params.id,
          route,
          city: {} as City,
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  private fetchNearbyCompanies = async (cityId: string) => {
    const companyResponse = await getPage(CompanyListUrl, {
      page: 0,
      size: 5,
      cityId,
    });

    if (companyResponse && companyResponse.content) {
      return companyResponse.content;
    }

    return [];
  }

  private renderCityLink = (cityId: string) => {
    if (cityId) {
      const { city } = this.state;
      return (
        <tr>
          <td>City:</td>
          <td><Link to={`/cities/${cityId}`}>{city.name}</Link></td>
        </tr>
      );
    }
    return '';
  }

  render() {
    const { route, id, nearbyCompanies } = this.state;
    const {
      city, name, vehicleType, bikesAllowed, wheelchairAccessible,
    } = route;

    if (id === '') {
      return <div />;
    }

    return (
      <Container>
        <Row className="m-3">
          <Col><h1>{name}</h1></Col>
        </Row>
        <Row className="m-3">
          <Col>
            <Container className="p-3 border rounded-lg bg-light">
              <h3>Information</h3>
              <Table>
                <tbody>
                  {this.renderCityLink(city.id)}
                  <tr>
                    <td>Vehicle type:</td>
                    <td>{formatData(vehicleType)}</td>
                  </tr>
                  <tr>
                    <td>Bikes allowed:</td>
                    <td>{formatData(bikesAllowed)}</td>
                  </tr>
                  <tr>
                    <td>Wheelchair accessible:</td>
                    <td>{formatData(wheelchairAccessible)}</td>
                  </tr>
                </tbody>
              </Table>
            </Container>
          </Col>
        </Row>
        <Row className="m-3">
          <Col>
            <Container className="p-3 border rounded-lg bg-light">
              <h3>Nearby Companies</h3>
              <ul>
                {nearbyCompanies.map((company) => (
                  <li>
                    <Link key={company.id} to={`/companies/${company.id}`}>{company.name}</Link>
                  </li>
                ))}
              </ul>
            </Container>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default TransitRouteInstance;
