import React from 'react';
import styled from 'styled-components';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { Link } from 'react-router-dom';
import splashGraphic from '../static/working.svg';

const HomePageWrapper = styled.div`
  margin-top: 200px;
`;

const LandingGraphic = styled.img`
  width: 75%;
`;

const LandingButton = styled(Button)`
  margin-top: 15px;
`;

class HomePage extends React.Component {
  render() {
    return (
      <HomePageWrapper>
        <Row className="d-flex align-items-center">
          <Col md={6} lg={6} sm="auto">
            <h1>Work for companies that drive positive change.</h1>
            <Row className="d-flex align-items-stretch">
              <Link to="/companies">
                <LandingButton
                  size="lg"
                  variant="primary"
                >
                  Browse companies
                </LandingButton>
              </Link>
            </Row>
            <Row>
              <Link to="/about">
                <LandingButton
                  size="md"
                  variant="light"
                >
                  Learn more
                </LandingButton>
              </Link>
            </Row>
          </Col>
          <Col md={6} lg={6} sm="auto" className="d-flex justify-content-center">
            <LandingGraphic src={splashGraphic} alt="Working" />
          </Col>
        </Row>
      </HomePageWrapper>
    );
  }
}

export default HomePage;
