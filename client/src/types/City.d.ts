type CityMetric = {
  name: string;
  // eslint-disable-next-line camelcase
  scoreOutOf10: number;
};

type Coordinate = {
  latitude: number;
  longitude: number;
};

type City = {
  id: string;
  name: string;
  country: string;
  latlon: Coordinate;
  population: number;
  metrics: CityMetric[];
  summary: string;
  overallScore: number;
  headline?: string;
  [index: string]: any;
};

export { Coordinate, CityMetric };
export default City;
