type FilterInstanceOption = {
  key: string;
  text: string;
}

type FilterInstance = {
  title: string;
  optionsKey: string;
  options: FilterInstanceOption[];
}

export { FilterInstanceOption };
export default FilterInstance;
