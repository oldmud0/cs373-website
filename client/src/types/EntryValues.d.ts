type EntryValues = {
  name: string;
  values: string[];
  path: string;
}

export default EntryValues;
