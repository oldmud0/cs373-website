import City from './City';

type Point = {
  x: number;
  y: number;
};

type TransitStop = {
  id: string;
  name: string;
};

type TransitRoute = {
  id: string;
  city: City;
  onestopId: string;
  name: string;
  vehicleType: string;
  stops: TransitStop[];
  bikesAllowed: string;
  wheelchairAccessible: string;
  operator: string;
  operatorUrl: string;
  geometry: Point[];
  headline?: string;
}

export default TransitRoute;
