import { Coordinate } from './City';

type PerformanceDetail = {
  min: number;
  avg: number;
  max: number;
}

type ScoreDetail = {
  fmt: string;
  raw: number;
}

type PercentileDetail = {
  fmt: string;
  raw: number;
}

type ESGScoreDetail = {
  score: ScoreDetail;
  percentile: PercentileDetail;
  performance: PerformanceDetail;
}

type ControversyDetail = {
  highestControversy: number;
  performance: PerformanceDetail;
  relatedControversy: string[];
}

type ESGScoreObject = {
  adult: boolean;
  alcoholic: boolean;
  animalTesting: boolean;
  catholic: boolean;
  coal: boolean;
  controversialWeapons: boolean;
  furLeather: boolean;
  gambling: boolean;
  gmo: boolean;
  militaryContract: boolean;
  nuclear: boolean;
  palmOil: boolean;
  pesticides: boolean;
  smallArms: boolean;

  environmentDetail: ESGScoreDetail;
  governanceDetail: ESGScoreDetail;
  socialDetail: ESGScoreDetail;
  esgDetail: ESGScoreDetail;

  controversyDetail: ControversyDetail;

  ratingMonth: number;
  ratingYear: number;
}

type Address = {
  street: string;
  city: string;
  state: string;
  country: string;
}

type Company = {
  id: string;
  name: string;
  summary: string;
  address: Address;
  nemployees: number;
  website: string;
  location: Coordinate;
  cityId: string;
  industry: string;
  esgScores: ESGScoreObject;
  headline?: string;
};

export default Company;
export {
  Address, ESGScoreDetail, ESGScoreObject, ControversyDetail,
};
