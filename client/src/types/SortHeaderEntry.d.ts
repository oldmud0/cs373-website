type SortHeaderEntry = {
  label: string;
  value?: string;
}

export default SortHeaderEntry;
