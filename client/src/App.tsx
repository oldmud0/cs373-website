import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import Directory from './pages/directories/Directory';
import CityInstance from './pages/instances/CityInstance';
import CompanyInstance from './pages/instances/CompanyInstance';
import TransitRouteInstance from './pages/instances/TransitRouteInstance';
import NotFoundPage from './pages/NotFoundPage';
import SearchPage from './pages/SearchPage';
import VisualizationPage from './pages/Visualizations';

import {
  CityListUrl, CitySearchUrl, cityFilterOptions, citySortHeader,
  CompanyListUrl, CompanySearchUrl, companyFilterOptions, companySortHeader,
  TransitRouteListUrl, TransitRouteSearchUrl, routeFilterOptions, routeSortHeader,
} from './components/Constants/Constants';
import { cityParser, companyParser, transitRouteParser } from './utils/parsers';
import { cityFormatter, companyFormatter, routeFormatter } from './utils/formatters';
import { SortOrder } from './components/Sorting/SortableTableHeader';

export const DIRECTORY_PAGE_SIZE: number = 15;

type AppState = {
  toastShowing: boolean;
}

class App extends React.Component<{}, AppState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      toastShowing: true,
    };
  }

  private onToastClick = () => {
    const { toastShowing } = this.state;
    this.setState({
      toastShowing: !toastShowing,
    });
  }

  render() {
    const { toastShowing } = this.state;

    return (
      <Router>
        <Navbar />
        <Container>
          <Switch>
            <Route path="/" exact>
              <HomePage />
            </Route>
            <Route path="/about" exact>
              <AboutPage />
            </Route>
            <Route
              path="/cities"
              exact
              component={() => (
                <Directory
                  pageSize={DIRECTORY_PAGE_SIZE}
                  filterInstance={cityFilterOptions}
                  defaultSort="population"
                  defaultSortOrder={SortOrder.DESC}
                  parser={cityParser}
                  searchUrl={CitySearchUrl}
                  listUrl={CityListUrl}
                  header="City Directory"
                  sortHeader={citySortHeader}
                  formatter={cityFormatter}
                />
              )}
            />
            <Route
              path="/companies"
              exact
              component={() => (
                <Directory
                  pageSize={DIRECTORY_PAGE_SIZE}
                  filterInstance={companyFilterOptions}
                  defaultSort="name"
                  defaultSortOrder={SortOrder.ASC}
                  parser={companyParser}
                  searchUrl={CompanySearchUrl}
                  listUrl={CompanyListUrl}
                  header="Company Directory"
                  sortHeader={companySortHeader}
                  formatter={companyFormatter}
                />
              )}
            />
            <Route
              path="/transit"
              exact
              component={() => (
                <Directory
                  pageSize={DIRECTORY_PAGE_SIZE}
                  filterInstance={routeFilterOptions}
                  defaultSort="name"
                  defaultSortOrder={SortOrder.ASC}
                  parser={transitRouteParser}
                  searchUrl={TransitRouteSearchUrl}
                  listUrl={TransitRouteListUrl}
                  header="Transit Route Directory"
                  sortHeader={routeSortHeader}
                  formatter={routeFormatter}
                />
              )}
            />
            <Route path="/search" exact>
              <SearchPage />
            </Route>
            <Route path="/visualizations" exact>
              <VisualizationPage />
            </Route>

            <Route path="/cities/:id" component={CityInstance} />
            <Route path="/companies/:id" component={CompanyInstance} />
            <Route path="/transit/:id" component={TransitRouteInstance} />
            <Route>
              <NotFoundPage />
            </Route>
          </Switch>
        </Container>
      </Router>
    );
  }
}

export default App;
