import React from 'react';
import { shallow } from 'enzyme';
import { createMemoryHistory, createLocation } from 'history';
import { match } from 'react-router-dom';
import { Entry } from '../../../components/Directory/Entry';
import { LinkedRow } from '../../../components/Constants/Constants';
import { cityFormatter } from '../../../utils/formatters';
import City from '../../../types/City';

const mockHistory = createMemoryHistory();
mockHistory.push = jest.fn();
const mockPath = '/route/:id';

const mockMatch: match<{ id: string }> = {
  isExact: false,
  path: mockPath,
  url: mockPath.replace(':id', '1'),
  params: { id: '1' },
};

const location = createLocation(mockMatch.url);

const mockCity: City = {
  id: '123',
  name: 'Mock City',
  country: 'USA',
  latlon: {
    latitude: 123,
    longitude: 456,
  },
  metrics: [{
    name: 'metric 1',
    scoreOutOf10: 8.4,
  }],
  summary: 'This is a city',
  overallScore: 55.5444444,
  population: 123,
};

describe('Entry component', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('renders without crashing', () => {
    const wrapper = shallow(
      <Entry
        entryValues={cityFormatter(mockCity)}
        history={mockHistory}
        match={mockMatch}
        location={location}
      />,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('calls history.push when clicked', () => {
    const wrapper = shallow(
      <Entry
        entryValues={cityFormatter(mockCity)}
        history={mockHistory}
        match={mockMatch}
        location={location}
      />,
    );

    wrapper.find(LinkedRow).simulate('click');

    expect(mockHistory.push).toHaveBeenCalledTimes(1);
  });

  it('rounds overallScore to 2 decimal places', () => {
    const wrapper = shallow(
      <Entry
        entryValues={cityFormatter(mockCity)}
        history={mockHistory}
        match={mockMatch}
        location={location}
      />,
    );

    const td = wrapper.find('td').at(4);

    expect(td.text()).toBe('55.54');
  });
});
