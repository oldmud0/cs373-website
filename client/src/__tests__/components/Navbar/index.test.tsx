import React from 'react';
import { shallow } from 'enzyme';
import Navbar from '../../../components/Navbar';

describe('Navbar component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Navbar />);

    expect(wrapper).toMatchSnapshot();
  });
});
