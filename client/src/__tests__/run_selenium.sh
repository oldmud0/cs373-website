#!/bin/bash
set -e

# if [ $CI ]; then
#     echo "Installing geckodriver and Firefox"

#     apt-get -y update
#     apt-get install -y jq firefox-esr
#     install_dir="/usr/local/bin"
#     json=$(curl -s https://api.github.com/repos/mozilla/geckodriver/releases/latest)
#     if [[ $(uname) == "Darwin" ]]; then
#         url=$(echo "$json" | jq -r '.assets[].browser_download_url | select(contains("macos"))')
#     elif [[ $(uname) == "Linux" ]]; then
#         url=$(echo "$json" | jq -r '.assets[].browser_download_url | select(contains("linux64"))')
#     else
#         echo "can't determine OS"
#         exit 1
#     fi
#     echo "fetching geckodriver from $url"
#     curl -s -L "$url" | tar -xz
#     chmod +x geckodriver
#     mv geckodriver "$install_dir"
#     echo "installed geckodriver binary in $install_dir"
# fi

# if [ $CI ]; then
#     # npm install chromedriver --chromedriver-force-download
#     wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
#     echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list
#     apt-get -y update
#     apt-get install -y default-jre
#     apt-get install -y google-chrome-stable
# fi

npx react-scripts test --watchAll=false --testPathPattern=selenium

echo "Selenium tests finished."
