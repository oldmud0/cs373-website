import React from 'react';
import { shallow } from 'enzyme';
import { createMemoryHistory, createLocation } from 'history';
import { match } from 'react-router-dom';
import City from '../../../types/City';
import CityInstance from '../../../pages/instances/CityInstance';
import * as requests from '../../../utils/request';

const mockHistory = createMemoryHistory();
const mockPath = '/route/:id';

const mockMatch: match<{ id: string }> = {
  isExact: false,
  path: mockPath,
  url: mockPath.replace(':id', '123'),
  params: { id: '123' },
};

const location = createLocation(mockMatch.url);

const mockCity: City = {
  id: '123',
  name: 'Mock City',
  country: 'USA',
  latlon: {
    latitude: 123,
    longitude: 456,
  },
  metrics: [{
    name: 'metric 1',
    scoreOutOf10: 8.4,
  }],
  summary: 'This is a city',
  overallScore: 55.5444444,
  population: 123,
};

describe('CityInstance page', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(
      <CityInstance
        history={mockHistory}
        match={mockMatch}
        location={location}
      />,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('calls instanceRequest and sets state on mount', async () => {
    (requests as any).instanceRequest = jest
      .fn()
      .mockImplementation(() => Promise.resolve(mockCity));

    const wrapper = shallow(
      <CityInstance
        history={mockHistory}
        match={mockMatch}
        location={location}
      />,
    );

    // force promises to run
    await Promise.resolve();
    expect(requests.instanceRequest).toHaveBeenCalledTimes(1);
    expect(wrapper.state('city')).toBe(mockCity);
  });
});
