import React from 'react';
import { shallow } from 'enzyme';
import City from '../../../types/City';
import * as requests from '../../../utils/request';
import Directory from '../../../pages/directories/Directory';
import { DIRECTORY_PAGE_SIZE } from '../../../App';
import {
  CityListUrl, CitySearchUrl, cityFilterOptions, citySortHeader,
} from '../../../components/Constants/Constants';
import { cityParser } from '../../../utils/parsers';
import { cityFormatter } from '../../../utils/formatters';
import { SortOrder } from '../../../components/Sorting/SortableTableHeader';

const mockCities: City[] = [
  {
    id: '123',
    name: 'Mock City 1',
    country: 'USA',
    latlon: {
      latitude: 123,
      longitude: 456,
    },
    metrics: [{
      name: 'metric 1',
      scoreOutOf10: 8.4,
    }],
    summary: 'This is a city',
    overallScore: 55.5444444,
    population: 123,
  },
  {
    id: '124',
    name: 'Mock City 2',
    country: 'USA',
    latlon: {
      latitude: 124,
      longitude: 766,
    },
    metrics: [{
      name: 'metric 1',
      scoreOutOf10: 1.4,
    }],
    summary: 'This is another city',
    overallScore: 15.5444444,
    population: 1235,
  },
];

describe('City directory page', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(
      <Directory
        pageSize={DIRECTORY_PAGE_SIZE}
        filterInstance={cityFilterOptions}
        defaultSort="population"
        defaultSortOrder={SortOrder.DESC}
        parser={cityParser}
        searchUrl={CitySearchUrl}
        listUrl={CityListUrl}
        header="City Directory"
        sortHeader={citySortHeader}
        formatter={cityFormatter}
      />,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('tries to get initial page on mount', async () => {
    (requests as any).getPage = jest
      .fn()
      .mockResolvedValue({
        totalPages: 10,
        content: mockCities,
      });

    const wrapper = shallow(
      <Directory
        pageSize={DIRECTORY_PAGE_SIZE}
        filterInstance={cityFilterOptions}
        defaultSort="population"
        defaultSortOrder={SortOrder.DESC}
        parser={cityParser}
        searchUrl={CitySearchUrl}
        listUrl={CityListUrl}
        header="City Directory"
        sortHeader={citySortHeader}
        formatter={cityFormatter}
      />,
    );

    await Promise.resolve();
    expect(requests.getPage).toHaveBeenCalledTimes(1);
    expect(wrapper.state('entries')).toBe(mockCities);
    expect(wrapper.state('totalPages')).toBe(10);
  });
});
