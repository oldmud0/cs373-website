import {
  Builder, By, Key, until, WebDriver,
} from 'selenium-webdriver';
import { Options } from 'selenium-webdriver/chrome';

require('chromedriver');

jest.setTimeout(60000);

const rootURL = 'https://ethicalemployers.club';
const builderOpts = new Options();
if (process.env.CI) {
  builderOpts.headless();
  builderOpts.addArguments('--no-sandbox', '--disable-dev-shm-usage');
}

const d = new Builder()
  .forBrowser('chrome')
  .setChromeOptions(builderOpts)
  .build();
const waitUntilTime = 60000;

let driver: WebDriver;

async function getElementBy(by: By) {
  const el = await driver.wait(until.elementLocated(by), waitUntilTime);
  return driver.wait(until.elementIsVisible(el), waitUntilTime);
}

// Async helpers for element selection
async function getElementById(id: string) {
  return getElementBy(By.id(id));
}

async function getElementByXPath(xpath: string) {
  return getElementBy(By.xpath(xpath));
}

async function getElementByClass(clazz: string) {
  return getElementBy(By.className(clazz));
}

async function getNavLink(item: string) {
  return getElementByXPath(
    `//a[contains(@class, "nav-link") and contains(string(), "${item}")]`,
  );
}

async function getHomeNavLink() {
  return getElementByXPath(
    '//a[contains(@class, "navbar-brand") and contains(string(), "Ethical Employers")]',
  );
}


describe('UI acceptance tests', () => {
  afterAll(async () => {
    await driver.quit();
  });

  beforeAll(() => d.then(async (_d) => {
    driver = _d;

    await driver.manage().window().setPosition(0, 0);
    await driver.manage().window().setSize(1920, 1080);
    await driver.get(rootURL);
  }));

  beforeEach(async () => {
    await driver.get(rootURL);
  });

  it('Clicking on home page button navigates to /companies', async () => {
    const homePageBtn = await getElementByXPath('//button');

    await homePageBtn.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/companies`);
  });

  it('Clicking on home page about button navigates to /about', async () => {
    const homePageBtn = await getElementByXPath('(//button)[2]');

    await homePageBtn.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/about`);
  });

  it('Should be able to navigate back and forth between nav links', async () => {
    const companiesNavLink = await getNavLink('Companies');
    await companiesNavLink.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/companies`);

    const homeLink = await getHomeNavLink();
    await homeLink.click();

    expect((await driver.getCurrentUrl()).startsWith(rootURL)).toBe(true);
  });

  it('Should be able to navigate to about page', async () => {
    const aboutNavLink = await getNavLink('About');
    await aboutNavLink.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/about`);
  });

  it('Should be able to navigate to companies directory page', async () => {
    const nav = await getNavLink('Companies');
    await nav.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/companies`);
  });

  it('Should be able to navigate to company instance page', async () => {
    const companiesNavLink = await getNavLink('Companies');
    await companiesNavLink.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/companies`);

    const firstCompanyRow = await getElementByXPath('.//tbody/tr/td');

    // TODO: figure out why getText doesn't work
    const firstCompanyName = await firstCompanyRow.getAttribute('innerHTML');
    await firstCompanyRow.click();

    expect((await driver.getCurrentUrl()).match(/.*companies\/.*$/)).toBeTruthy();
  });

  it('Should be able to navigate to routes directory page', async () => {
    const transitNavLink = await getNavLink('Transit Routes');
    await transitNavLink.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/transit`);
  });

  it('Should be able to navigate to route instance page', async () => {
    const nav = await getNavLink('Transit Routes');
    await nav.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/transit`);

    const firstRouteRow = await getElementByXPath('.//tbody/tr/td');
    await firstRouteRow.click();

    expect((await driver.getCurrentUrl()).match(/.*transit\/.*$/)).toBeTruthy();
  });

  it('Should be able to navigate to cities directory page', async () => {
    const nav = await getNavLink('Cities');
    await nav.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/cities`);
  });

  it('Should be able to navigate to city instance page', async () => {
    const nav = await getNavLink('Cities');
    await nav.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/cities`);

    const firstCityRow = await getElementByXPath('.//tbody/tr/td');
    await firstCityRow.click();

    expect((await driver.getCurrentUrl()).match(/.*cities\/.*$/)).toBeTruthy();
  });

  it('Should be able to navigate to search page', async () => {
    const nav = await getNavLink('Search');
    await nav.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/search`);
  });

  it('Should be able to get search results', async () => {
    const nav = await getNavLink('Search');
    await nav.click();

    expect(await driver.getCurrentUrl()).toBe(`${rootURL}/search`);

    const searchBar = await getElementByXPath('.//input');
    searchBar.sendKeys('austin', Key.ENTER);

    const firstResult = await getElementByClass('search-highlighted');
    expect(firstResult).toBeTruthy();
  });
});
