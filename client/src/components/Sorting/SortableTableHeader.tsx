import React from 'react';
import styled from 'styled-components';
import arrow from '../../static/down-arrow.svg';

type SortableTableHeaderProps = {
  onClick: () => any;
  sortOrder: number;
  active: boolean;
  children: React.ReactNode;
}

type ArrowIconProps = {
  active: boolean;
  down?: boolean;
}

const IconWrapper = styled.div`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  width: 15px;
  margin-left: 10px;
`;

const ArrowIcon = styled.img`
  margin-left: auto;
  height: 15px;
  width: 15px;
  opacity: ${(props: ArrowIconProps) => (props.active ? 1.0 : 0.5)};
  transform: ${(props: ArrowIconProps) => (props.down ? 'none' : 'rotate(180deg)')};
`;

const ThContentWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export const StyledTh = styled.th`
  cursor: pointer;
`;

const SortableTableHeader: React.FC<SortableTableHeaderProps> = ({
  onClick,
  sortOrder,
  active,
  children,
}) => (
  <StyledTh onClick={onClick}>
    <ThContentWrapper>
      {children}
      <IconWrapper>
        <ArrowIcon src={arrow} active={active && sortOrder === 1} />
        <ArrowIcon src={arrow} active={active && sortOrder === -1} down />
      </IconWrapper>
    </ThContentWrapper>
  </StyledTh>
);

export const SortOrder = {
  ASC: 1,
  DESC: -1,
};

export default SortableTableHeader;
