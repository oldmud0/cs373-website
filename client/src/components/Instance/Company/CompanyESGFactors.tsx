import React from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import { ESGScoreObject } from '../../../types/Company';

const formatKey = (key: string) => key.split(/(?=[A-Z])/)
  .map((word) => `${word.charAt(0).toUpperCase()}${word.substr(1)}`)
  .join(' ');

class CompanyESGFactors extends React.Component<{ esgScores: ESGScoreObject }, {}> {
  render() {
    const { esgScores } = this.props;
    const entries = Object.entries(esgScores).sort();

    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>ESG Score Factors</h3>
        <Container className="border rounded-lg overflow-auto bg-white" style={{ maxHeight: '285px' }}>
          <Table>
            <tbody>
              {entries.map(([key, value]) => {
                if (key && typeof value === 'boolean') {
                  return (
                    <tr key={key}>
                      <td>{formatKey(key)}</td>
                      <td>{String(value)}</td>
                    </tr>
                  );
                }
                return '';
              })}
            </tbody>
          </Table>
        </Container>
      </Container>
    );
  }
}

export default CompanyESGFactors;
