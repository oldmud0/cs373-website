import React from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import { Link } from 'react-router-dom';
import Company from '../../../types/Company';

class CompanyInfo extends React.Component<{ company: Company }, {}> {
  render() {
    const { company } = this.props;
    const {
      cityId, website, address, industry, nemployees,
    } = company;
    const {
      street, city, state, country,
    } = address;

    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>Company Info</h3>
        <Table>
          <tbody>
            <tr>
              <th>Website:</th>
              <td><a href={website}>{ website }</a></td>
            </tr>
            <tr>
              <th>Address:</th>
              <td>
                { street }{', '}
                { cityId ? <Link to={`/cities/${cityId}`}>{city}</Link> : city}{', '}
                { state }{', '}
                { country}
              </td>
            </tr>
            <tr>
              <th>Industry:</th>
              <td>{ industry }</td>
            </tr>
            <tr>
              <th>Full Time Employees:</th>
              <td>{ nemployees.toLocaleString() }</td>
            </tr>
          </tbody>
        </Table>
      </Container>
    );
  }
}

export default CompanyInfo;
