import React, { Ref } from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import Slider from '@material-ui/core/Slider';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { ControversyDetail } from '../../../types/Company';
import infoIcon from '../../../static/information.svg';
import { controversyExplanations } from '../../Constants/Constants';

type CompanyESGControversyPanelProps = {
  controversy: ControversyDetail;
}

const CompanyESGControversyPanel: React.FC<CompanyESGControversyPanelProps> = ({ controversy }) => {
  const { performance, relatedControversy } = controversy;
  const { avg } = performance;

  const marks = [
    { value: 0, label: '0' },
    { value: avg, label: `Avg: ${avg.toFixed(2)}` },
    { value: 5, label: '5' },
  ];

  return (
    <Container className="p-3 border rounded-lg bg-light">
      <h3>Controversies</h3>
      <Container>
        <Row className="mt-3">
          <Col><h6>Highest Controversy:</h6></Col>
        </Row>
        <Row className="mt-5">
          <Col>
            <Slider
              defaultValue={controversy.highestControversy}
              min={0}
              max={5}
              marks={marks}
              valueLabelDisplay="on"
              disabled
            />
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <h6>Related Controversies:</h6>
            <div style={{ minHeight: '100px', maxHeight: '100px', overflowY: 'scroll' }}>
              <Table>
                <tbody>
                  {relatedControversy.map((currControversy) => (
                    <tr key={currControversy}>
                      <td>
                        {currControversy}
                        <OverlayTrigger
                          placement="right"
                          overlay={(
                            <Tooltip id={`tooltip-${currControversy}`}>
                              <span>
                                {controversyExplanations[currControversy] || 'This is bad.'}
                              </span>
                            </Tooltip>
                          )}
                        >
                          <img
                            src={infoIcon}
                            height={15}
                            width={15}
                            alt="hover for more info"
                            style={{ margin: '5px' }}
                          />
                        </OverlayTrigger>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </Col>
        </Row>
      </Container>
    </Container>
  );
};

export default CompanyESGControversyPanel;
