import React from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import Slider from '@material-ui/core/Slider';
import { ESGScoreDetail } from '../../../types/Company';

class CompanyESGScorePanel extends React.Component
  <{ title: string, description: string, esgScoreDetail: ESGScoreDetail }, {}> {
  render() {
    const { title, description, esgScoreDetail } = this.props;
    const { score, percentile, performance } = esgScoreDetail;

    const marks = [
      { value: 0, label: '0' },
      { value: performance.avg, label: `Avg: ${performance.avg.toFixed(2)}` },
      { value: 100, label: '100' },
    ];

    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>{ title }</h3>
        <Container>
          <Row className="mt-3">
            <Col><h6>{ description }</h6></Col>
          </Row>
          <Row className="mt-3">
            <Col><h6>Score:</h6></Col>
          </Row>
          <Row className="mt-5">
            <Col>
              <Slider
                defaultValue={score.raw}
                min={0}
                max={100}
                marks={marks}
                valueLabelDisplay="on"
                disabled
              />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col>
              <h6>This score is within the the top {percentile.raw}% of scores</h6>
            </Col>
          </Row>
        </Container>
      </Container>
    );
  }
}

export default CompanyESGScorePanel;
