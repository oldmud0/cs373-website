import React from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import CityCompanyEntry from './CityCompanyEntry';
import { getPage, QueryParams } from '../../../utils/request';
import { CompanyListUrl } from '../../Constants/Constants';
import Company from '../../../types/Company';
import DirectoryPagination from '../../Directory/DirectoryPagination';

class CityCompanies extends React.Component
  <{ pageSize: number, id: string },
  { totalPages: number, companies: Company[] }> {
  state = {
    totalPages: 0,
    companies: [] as Company[],
  };

  componentDidMount() {
    this.switchPage({ page: 0 });
  }

  switchPage = (params: QueryParams) => {
    const { id, pageSize } = this.props;

    getPage(CompanyListUrl, {
      ...params,
      cityId: id,
      size: pageSize,
    })
      .then((page) => {
        this.setState({
          totalPages: page.totalPages,
          companies: page.content as Company[],
        });
      })
      .catch((error) => console.log);
  }

  render() {
    const { totalPages, companies } = this.state;

    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>Companies</h3>
        <Container className="border rounded-lg overflow-auto bg-white" style={{ minHeight: '220px', maxHeight: '220px' }}>
          <Table className="table-hover">
            <thead>
              <tr>
                <th>Company Name</th>
                <th>Industry</th>
              </tr>
            </thead>
            <tbody>
              {companies.map((company) => (
                <CityCompanyEntry
                  key={company.id}
                  id={company.id}
                  name={company.name}
                  industry={company.industry}
                />
              ))}
            </tbody>
          </Table>
        </Container>
        <DirectoryPagination totalPages={totalPages} callback={this.switchPage} />
      </Container>
    );
  }
}

export default CityCompanies;
