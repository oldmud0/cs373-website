import React from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import CityTransitRouteEntry from './CityTransitRouteEntry';
import { getPage, QueryParams } from '../../../utils/request';
import { TransitRouteListUrl } from '../../Constants/Constants';
import TransitRoute from '../../../types/TransitRoute';
import DirectoryPagination from '../../Directory/DirectoryPagination';

class CityTransitRoutes extends React.Component
  <{ pageSize: number, id: string },
  { totalPages: number, transitRoutes: TransitRoute[] }> {
  state = {
    totalPages: 0,
    transitRoutes: [] as TransitRoute[],
  };

  componentDidMount() {
    this.switchPage({ page: 0 });
  }

  switchPage = (params: QueryParams) => {
    const { id, pageSize } = this.props;

    getPage(TransitRouteListUrl, {
      ...params,
      size: pageSize,
      cityId: id,
    })
      .then((page) => {
        this.setState({
          totalPages: page.totalPages,
          transitRoutes: page.content as TransitRoute[],
        });
      })
      .catch(console.log);
  }

  render() {
    const { totalPages, transitRoutes } = this.state;

    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>Transit Routes</h3>
        <Container className="border rounded-lg overflow-auto bg-white" style={{ minHeight: '220px', maxHeight: '220px' }}>
          <Table className="table-hover">
            <thead>
              <tr>
                <th>Route Name</th>
              </tr>
            </thead>
            <tbody>
              {transitRoutes.map((transitRoute) => (
                <CityTransitRouteEntry
                  key={transitRoute.id}
                  id={transitRoute.id}
                  name={transitRoute.name}
                />
              ))}
            </tbody>
          </Table>
        </Container>
        <DirectoryPagination totalPages={totalPages} callback={this.switchPage} />
      </Container>
    );
  }
}

export default CityTransitRoutes;
