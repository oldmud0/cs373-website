import React from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import { CityMetric } from '../../../types/City';

class CityMetrics extends React.Component<{ metrics: CityMetric[] }, {}> {
  render() {
    const { metrics } = this.props;
    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>Metrics</h3>
        <Container className="border rounded-lg overflow-auto bg-white" style={{ maxHeight: '445px' }}>
          <Table>
            <thead>
              <tr>
                <th>Category</th>
                <th>Score out of 10</th>
              </tr>
            </thead>
            <tbody>
              {metrics.map((metric) => (
                <tr key={metric.name}>
                  <td>{ metric.name }</td>
                  <td>{ metric.scoreOutOf10.toFixed(2) }</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>
      </Container>
    );
  }
}

export default CityMetrics;
