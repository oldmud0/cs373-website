import React from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import City, { Coordinate } from '../../../types/City';


class CityBasicInfo extends React.Component<{ city: City }, {}> {
  render() {
    const { city } = this.props;
    const {
      country, latlon, population, overallScore,
    } = city;
    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>Basic Info</h3>
        <Table>
          <tbody>
            <tr>
              <th>Country:</th>
              <td>{country}</td>
            </tr>
            <tr>
              <th>Location:</th>
              <td>{`(${latlon.latitude}, ${latlon.longitude})`}</td>
            </tr>
            <tr>
              <th>Population:</th>
              <td>{population.toLocaleString()}</td>
            </tr>
            <tr>
              <th>Overall Score:</th>
              <td>{overallScore.toFixed(2)}</td>
            </tr>
          </tbody>
        </Table>
      </Container>
    );
  }
}

export default CityBasicInfo;
