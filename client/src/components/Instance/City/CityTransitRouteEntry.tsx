import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { LinkedRow } from '../../Constants/Constants';

class CityTransitRouteEntry extends React.Component
  <{ id: string, name: string} & RouteComponentProps, {}> {
  render() {
    const { id, name, history } = this.props;

    return (
      <LinkedRow onClick={() => history.push(`/transit/${id}`)}>
        <td>{name}</td>
      </LinkedRow>
    );
  }
}

export default withRouter(CityTransitRouteEntry);
