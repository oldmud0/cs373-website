import React from 'react';
import Container from 'react-bootstrap/Container';

class CitySummary extends React.Component<{ summary: string }, {}> {
  render() {
    const { summary } = this.props;
    return (
      <Container className="p-3 mt-3 border rounded-lg bg-light">
        <h3>Summary</h3>
        <Container className="p-3 border rounded-lg bg-white overflow-auto" style={{ minHeight: '140px', maxHeight: '140px' }}>
          <p>{ summary }</p>
        </Container>
      </Container>
    );
  }
}

export default CitySummary;
