import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { LinkedRow } from '../../Constants/Constants';

class CityCompanyEntry extends React.Component
  <{ id: string, name: string, industry: string } & RouteComponentProps, {}> {
  render() {
    const {
      id, name, industry, history,
    } = this.props;

    return (
      <LinkedRow onClick={() => history.push(`/companies/${id}`)}>
        <td>{name}</td>
        <td>{industry}</td>
      </LinkedRow>
    );
  }
}

export default withRouter(CityCompanyEntry);
