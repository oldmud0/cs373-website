import React from 'react';
import styled from 'styled-components';
import Spinner from 'react-bootstrap/Spinner';
import FilterInstance, { FilterInstanceOption } from '../../types/FilterInstance';
import SortHeaderEntry from '../../types/SortHeaderEntry';


export const API_BASE_URL = `${process.env.REACT_APP_API_ENDPOINT || 'https://api.ethicalemployers.club'}/v1`;
export const CityListUrl = `${API_BASE_URL}/city/list`;
export const CitySearchUrl = `${API_BASE_URL}/city/search`;
export const CityInstanceUrl = `${API_BASE_URL}/city/detail`;
export const CompanyListUrl = `${API_BASE_URL}/company/list`;
export const CompanySearchUrl = `${API_BASE_URL}/company/search`;
export const CompanyInstanceUrl = `${API_BASE_URL}/company/detail`;
export const TransitRouteListUrl = `${API_BASE_URL}/route/list`;
export const TransitRouteSearchUrl = `${API_BASE_URL}/route/search`;
export const TransitRouteInstanceUrl = `${API_BASE_URL}/route/detail`;

export const environmentScoreDescription: string = 'Measures the extent to which the firm promotes the sustained habitability of Earth’s environment.';
export const governanceScoreDescription: string = 'Measures the transparency of the firm’s organization and relations with its employees.';
export const socialScoreDescription: string = 'Measures how conscious the company is to topics such as employment diversity and consumer protection.';
export const esgScoreDescription: string = 'An aggregate Environmental, Social and Governance (ESG) score computed by MSCI Inc.';

export const controversyExplanations: { [key: string]: string; } = {
  'Environmental Supply Chain Incidents': 'An incident related to supply chain failures causing environmental damage.',
  'Employee Incidents': 'Incidents related to company employees, such as worker strikes and HR-related controversies.',
  'Operations Incidents': 'Incidents occurring as part of a company\' day-to-day operations.',
  'Public Policy Incidents': 'Incidents relating to controversial public policy topics.',
  'Governance Incidents': 'Exposed or investigated for unlawful practices of the company\'s governing body.',
  'Business Ethics Incidents': 'Exposed or investigated for breaches of business law.',
  'Social Supply Chain Incidents': 'Improper use or management of social data, including customer personal information and privacy breaches.',
  'Product & Service Incidents': 'Examples include product recalls and safety issues in consumer goods.',
  'Customer Incidents': 'A customer-related incident, such as a class-action lawsuit.',
  'Society & Community Incidents': 'An incident affecting entire communities where it occurred.',
};

export const companyFilterOptions: FilterInstance[] = [
  {
    title: 'Industry',
    optionsKey: 'industry',
    options: [
      { key: 'Technology', text: 'Technology' },
      { key: 'Real Estate', text: 'Real Estate' },
      { key: 'Healthcare', text: 'Healthcare' },
      { key: 'Basic Materials', text: 'Basic Materials' },
      { key: 'Industrials', text: 'Industrials' },
      { key: 'Energy', text: 'Energy' },
      { key: 'Consumer Cyclical', text: 'Consumer Cyclical' },
      { key: 'Utilities', text: 'Utilities' },
      { key: 'Consumer Defensive', text: 'Consumer Defensive' },
      { key: 'Financial Services', text: 'Financial Services' },
      { key: 'Communication Services', text: 'Communication Services' },
    ] as FilterInstanceOption[],
  },
];

export const cityFilterOptions: FilterInstance[] = [
  {
    title: 'Country',
    optionsKey: 'country',
    options: [
      { key: 'United States', text: 'United States' },
      { key: 'United Kingdom', text: 'United Kingdom' },
      { key: 'Canada', text: 'Canada' },
      { key: 'Switzerland', text: 'Switzerland' },
      { key: 'Hong Kong', text: 'Hong Kong' },
      { key: 'Philippines', text: 'Philippines' },
    ] as FilterInstanceOption[],
  },
];

export const routeFilterOptions: FilterInstance[] = [
  {
    title: 'Bikes Allowed',
    optionsKey: 'bikesAllowed',
    options: [
      { key: 'all_trips', text: 'All trips' },
      { key: 'some_trips', text: 'Some trips' },
      { key: 'no_trips', text: 'No trips' },
      { key: 'unknown', text: 'Unknown' },
    ] as FilterInstanceOption[],
  },
  {
    title: 'Vehicle Type',
    optionsKey: 'vehicleType',
    options: [
      { key: 'bus', text: 'Bus' },
      { key: 'metro', text: 'Metro' },
      { key: 'tram', text: 'Tram' },
      { key: 'ferry', text: 'Ferry' },
      { key: 'rail', text: 'Rail' },
      { key: 'cablecar', text: 'Cablecar' },
    ] as FilterInstanceOption[],
  },
  {
    title: 'Wheelchair Accessible',
    optionsKey: 'wheelchairAccessible',
    options: [
      { key: 'all_trips', text: 'All trips' },
      { key: 'some_trips', text: 'Some trips' },
      { key: 'no_trips', text: 'No trips' },
      { key: 'unknown', text: 'Unknown' },
    ] as FilterInstanceOption[],
  },
];

export const companySortHeader: SortHeaderEntry[] = [
  {
    label: 'City Name',
    value: 'name',
  },
  {
    label: 'Industry',
    value: 'industry',
  },
  {
    label: 'Environmental Score',
  },
  {
    label: 'Governance Score',
  },
  {
    label: 'Social Score',
  },
  {
    label: 'Total ESG Score',
  },
];

export const citySortHeader: SortHeaderEntry[] = [
  {
    label: 'City Name',
    value: 'name',
  },
  {
    label: 'Country',
    value: 'country',
  },
  {
    label: 'Location',
  },
  {
    label: 'Population',
    value: 'population',
  },
  {
    label: 'Overall Score',
    value: 'overallScore',
  },
];

export const routeSortHeader: SortHeaderEntry[] = [
  {
    label: 'Route Number',
    value: 'name',
  },
  {
    label: 'City',
    value: 'city.name',
  },
  {
    label: 'Vehicle Type',
    value: 'vehicleType',
  },
  {
    label: 'Bikes Allowed',
    value: 'bikesAllowed',
  },
  {
    label: 'Wheelchair Accessible',
    value: 'wheelchairAccessible',
  },
];

export const DirectoryTableWrapper = styled.div`
  min-height: 75vh;
  max-height: 75vh;
`;

export const FixedTableHeader = styled.thead`
  th {
    position: sticky;
    top: 0;
    background-color: white;
    border-bottom: 1px solid grey;
  }
`;

export const LinkedRow = styled.tr`
  cursor: pointer;
`;

export const Loader = () => (
  <Spinner animation="border" role="status">
    <span className="sr-only">Loading...</span>
  </Spinner>
);
