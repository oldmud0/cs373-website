import React from 'react';
import Pagination from 'react-bootstrap/Pagination';
import { QueryParams } from '../../utils/request';

const createNewPages = (newPage: number, totalPages: number): number[] => {
  const page: number = Math.max(0, Math.min(totalPages - 1, newPage));
  const minPage: number = Math.max(0, page - 1);
  const maxPage: number = Math.min(totalPages - 1, page + 1);
  const newPages: number[] = [];

  if (page === (totalPages - 1) && (minPage - 1) >= 0) {
    newPages.push(minPage - 1);
  }

  for (let i = minPage; i <= maxPage; i += 1) {
    newPages.push(i);
  }

  if (page === 0 && (maxPage + 1) < totalPages) {
    newPages.push(maxPage + 1);
  }

  return newPages;
};

class DirectoryPagination extends React.Component
  <{ totalPages: number, callback: (params: QueryParams) => void },
  { activePage: number, pages: number[] }> {
  constructor(props: any) {
    super(props);
    const { totalPages } = this.props;
    this.state = {
      activePage: 0,
      pages: createNewPages(0, totalPages),
    };
  }

  componentDidUpdate(prevProps: any) {
    const { activePage } = this.state;
    const { totalPages } = this.props;

    if (totalPages !== prevProps.totalPages) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        pages: createNewPages(activePage, totalPages),
      });
    }
  }

  switchPage(newPage: number, total?: number) {
    const { totalPages, callback } = this.props;

    const page: number = Math.max(0, Math.min(totalPages - 1, newPage));

    this.setState({
      activePage: page,
      pages: createNewPages(page, totalPages),
    }, () => callback({ page }));
  }

  render() {
    const { activePage, pages } = this.state;
    const { totalPages } = this.props;

    return (
      <Pagination className="justify-content-center" style={{ margin: '15px' }}>
        <Pagination.First
          disabled={activePage < 2}
          onClick={
            () => this.switchPage(0)
          }
        />
        <Pagination.Prev
          disabled={activePage < 2}
          onClick={
            () => this.switchPage(activePage - 2)
          }
        />
        {pages.map((page) => (
          <Pagination.Item
            key={page}
            active={activePage === page}
            onClick={
              () => this.switchPage(page)
            }
          >{page + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next
          disabled={activePage > (totalPages - 3)}
          onClick={
            () => this.switchPage(activePage + 2)
          }
        />
        <Pagination.Last
          disabled={activePage > (totalPages - 3)}
          onClick={
            () => this.switchPage(totalPages - 1)
          }
        />
      </Pagination>
    );
  }
}

export default DirectoryPagination;
