import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';

class DirectoryHeader extends React.Component
  <{ header: string, callback: (value: string) => void }, { value: string }> {
  state = {
    value: '',
  }

  textboxRef: any = React.createRef();

  handleChange = (e: any) => {
    this.setState({
      value: e.target.value,
    });
  }

  handleKeyDown = (e: any) => {
    const { callback } = this.props;
    const { value } = this.state;
    if (e.key === 'Enter') {
      e.preventDefault();
      callback(value);
    }
  }

  handleSearch = () => {
    const { callback } = this.props;
    const { value } = this.state;

    callback(value);
  }

  clearSearch = () => {
    const { current } = this.textboxRef;
    current.value = '';
  }

  render() {
    const { header } = this.props;

    return (
      <Navbar className="justify-content-between">
        <Navbar.Brand><h1>{ header }</h1></Navbar.Brand>
        <Form>
          <InputGroup>
            <FormControl
              ref={this.textboxRef}
              type="text"
              placeholder="Search"
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
            />
            <InputGroup.Append>
              <Button onClick={this.handleSearch}>Search</Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </Navbar>
    );
  }
}

export default DirectoryHeader;
