/* eslint-disable react/no-danger */
import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { LinkedRow } from '../Constants/Constants';
import highlight from '../../utils/highlight';
import EntryValues from '../../types/EntryValues';

type EntryProps = {
  entryValues: EntryValues;
  searchQuery?: string;
} & RouteComponentProps;

export const Entry: React.FC<EntryProps> = ({ history, entryValues, searchQuery }) => {
  const {
    path, name, values,
  } = entryValues;

  if (!values.length) {
    return <div />;
  }

  return (
    <LinkedRow onClick={() => history.push(path)}>
      <td
        dangerouslySetInnerHTML={{
          __html: highlight(name, searchQuery),
        }}
      />
      {values.map((value) => (
        <td>{value}</td>
      ))}
    </LinkedRow>
  );
};

export default withRouter(Entry);
