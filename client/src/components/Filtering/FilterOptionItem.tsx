import React from 'react';
import styled from 'styled-components';
import Form from 'react-bootstrap/Form';

export type FilterOptionItemProps = {
  name: string;
  active: boolean;
  text: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const FilterOptionItemWrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
  padding-left: 10px;
`;

const FilterOptionItem: React.FC<FilterOptionItemProps> = ({
  name, active, text, onChange,
}) => (
  <FilterOptionItemWrapper>
    <Form.Check name={name} checked={active} onChange={onChange} />
    <span>
      {text}
    </span>
  </FilterOptionItemWrapper>
);

export default FilterOptionItem;
