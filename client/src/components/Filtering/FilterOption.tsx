import React, { ChangeEvent } from 'react';
import styled from 'styled-components';
import Form from 'react-bootstrap/Form';
import Dropdown from 'react-bootstrap/Dropdown';
import FilterOptionItem, { FilterOptionItemProps } from './FilterOptionItem';

const FilterOptionWrapper = styled.div`
  margin-right: 10px;
`;

export type FilterOptionProps = {
  title: string;
  optionsKey: string;
  options: FilterSuboption[];
  onChange: (filterName: string, option: string, active: boolean) => void;
}

type FilterSuboption = {
  key: string;
  text: string;
  active?: boolean;
}

type FilterOptionState = {
  options: FilterSuboption[];
}

class FilterOption extends React.Component<FilterOptionProps, FilterOptionState> {
  constructor(props: FilterOptionProps) {
    super(props);

    this.state = {
      options: props.options.map(({ key, text }) => ({
        key,
        text,
        active: false,
      })),
    };
  }

  resetFilter = () => {
    const { options } = this.props;
    this.setState({
      options: options.map(({ key, text }) => ({
        key,
        text,
        active: false,
      })),
    });
  }

  private handleOptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { onChange, title, optionsKey } = this.props;
    const { options } = this.state;

    const changedKey = e.currentTarget.name;
    let changedStatus = false;

    const toggledOptions = options.map((opt) => {
      if (e.currentTarget.name === opt.key) {
        changedStatus = !opt.active;
        return { ...opt, active: !opt.active };
      }
      return opt;
    });

    this.setState({
      options: toggledOptions,
    });

    onChange(optionsKey, changedKey, changedStatus);
  }

  render() {
    const { title } = this.props;
    const { options } = this.state;

    return (
      <FilterOptionWrapper>
        <Dropdown>
          <Dropdown.Toggle variant="light" id="dropdown-basic">
            {title}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            {options.map((opt) => (
              <Dropdown.Item key={opt.key}>
                <Form.Check
                  name={opt.key}
                  checked={opt.active}
                  onChange={this.handleOptionChange}
                  label={opt.text}
                />
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      </FilterOptionWrapper>
    );
  }
}

export default FilterOption;
