import React from 'react';
import styled from 'styled-components';
import FilterOption from './FilterOption';

const FilterWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  margin-bottom: 25px;
  padding: 5px;
`;

type FilterProps = {
  children: React.ReactNode;
}

const Filter: React.FC<FilterProps> = ({ children }) => (
  <FilterWrapper>
    {children}
  </FilterWrapper>
);

export {
  FilterOption,
};

export default Filter;
