import React from 'react';
import styled from 'styled-components';
import Company from '../../../types/Company';
import EsgPlot from './EsgPlot';

const Wrapper = styled.div`
  margin-top: 50px;
`;

type EsgScoreVisualizationProps = {
  companies: Company[];
  height: number;
  width: number;
}

type EsgScoreVisualizationState = {
  exampleCompanies: Company[];
}

class EsgScoreVisualization extends React.Component
  <EsgScoreVisualizationProps, EsgScoreVisualizationState> {
  constructor(props: EsgScoreVisualizationProps) {
    super(props);

    const examples = [
      'facebook',
      'microsoft',
      '3m company',
      'boeing',
      'pacific',
      'cvs',
      'paypal',
    ];
    this.state = {
      exampleCompanies: examples
        .map((name) => props.companies
          .find((company) => company.name.toLowerCase().startsWith(name))!),
    };
  }

  render() {
    const { companies, height, width } = this.props;
    const { exampleCompanies } = this.state;

    return (
      <Wrapper>
        <h3>ESG score comparison by industry</h3>
        <EsgPlot companies={companies} height={height} width={width} examples={exampleCompanies} />
      </Wrapper>
    );
  }
}

export default EsgScoreVisualization;
