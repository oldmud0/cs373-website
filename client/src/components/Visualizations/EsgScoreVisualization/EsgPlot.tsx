import React from 'react';
import * as d3 from 'd3';
import Company from '../../../types/Company';

type EsgDatum = {
  name: string;
  industry: string;
  score: number;
}

type EsgStatistic = {
  q1: number;
  median: number;
  q3: number;
  iqr: number;
  min: number;
  max: number;
}

type EsgPlotProps = {
  height: number;
  width: number;
  companies: Company[];
  examples: Company[];
}

class EsgPlot extends React.Component<EsgPlotProps, {}> {
  ref!: HTMLDivElement;

  margin!: {[k: string]: number};

  height!: number;

  width!: number;

  x!: d3.ScaleBand<string>;

  y!: d3.ScaleLinear<number, number>;

  svg!: d3.Selection<SVGGElement, unknown, HTMLElement, any>;

  constructor(props: EsgPlotProps) {
    super(props);

    this.margin = {
      top: 10, right: 30, bottom: 150, left: 40,
    };

    this.width = props.width - this.margin.left - this.margin.right;
    this.height = props.height - this.margin.top - this.margin.bottom;
  }

  componentDidMount() {
    const { margin, height, width } = this;
    const { companies } = this.props;

    const svg = d3.select('#esg-plot')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform',
        `translate(${margin.left},${margin.top})`);
    this.svg = svg;

    const x = d3.scaleBand()
      .range([0, width])
      .domain([
        'Real Estate',
        'Healthcare',
        'Basic Materials',
        'Industrials',
        'Energy',
        'Consumer Cyclical',
        'Utilities',
        'Technology',
        'Consumer Defensive',
        'Financial Services',
        'Communication Services',
      ])
      .paddingInner(1)
      .paddingOuter(0.5);
    this.x = x;
    svg.append('g')
      .attr('transform', `translate(0,${height})`)
      .call(d3.axisBottom(x))
      .selectAll('text')
      .attr('transform', 'translate(-10,0)rotate(-45)')
      .attr('font-size', '14px')
      .style('text-anchor', 'end');

    const y = d3.scaleLinear()
      .domain([0, 100])
      .range([height, 0]);
    this.y = y;
    svg.append('g').call(d3.axisLeft(y));

    this.drawPlot();
    this.drawExamples();
  }

  private drawPlot = () => {
    const { companies } = this.props;

    const stats = d3.nest<EsgDatum, EsgStatistic>()
      .key((d: any) => d.industry)
      .rollup((d: any) => {
        const q1 = d3.quantile(d.map((g: any) => g.score).sort(d3.ascending), 0.25) || 0;
        const median = d3.quantile(d.map((g: any) => g.score).sort(d3.ascending), 0.5) || 0;
        const q3 = d3.quantile(d.map((g: any) => g.score).sort(d3.ascending), 0.75) || 0;
        const iqr = q3 - q1;
        const min = q1 - 1.5 * iqr;
        const max = q3 + 1.5 * iqr;
        return ({
          q1, median, q3, iqr, min, max,
        });
      })
      .entries(companies.filter(this.verify).map(this.simplify));

    const { svg } = this;
    svg
      .selectAll('vertLines')
      .data(stats)
      .enter()
      .append('line')
      .attr('x1', (d: any) => (this.x(d.key)!))
      .attr('x2', (d: any) => (this.x(d.key)!))
      .attr('y1', (d: any) => (this.y(d.value.min)))
      .attr('y2', (d: any) => (this.y(d.value.max)))
      .attr('stroke', 'black')
      .style('width', 40);

    // rectangle for the main box
    const boxWidth = 25;
    svg
      .selectAll('boxes')
      .data(stats)
      .enter()
      .append('rect')
      .attr('x', (d: any) => (this.x(d.key)! - boxWidth / 2))
      .attr('y', (d: any) => (this.y(d.value.q3)))
      .attr('height', (d: any) => (this.y(d.value.q1) - this.y(d.value.q3)))
      .attr('width', boxWidth)
      .attr('stroke', 'black')
      .style('fill', 'lightblue');

    // Show the median
    svg
      .selectAll('medianLines')
      .data(stats)
      .enter()
      .append('line')
      .attr('x1', (d: any) => (this.x(d.key)! - boxWidth / 2))
      .attr('x2', (d: any) => (this.x(d.key)! + boxWidth / 2))
      .attr('y1', (d: any) => (this.y(d.value.median)))
      .attr('y2', (d: any) => (this.y(d.value.median)))
      .attr('stroke', 'black')
      .style('width', 80);
  }

  private drawExamples = () => {
    const { examples } = this.props;
    const { svg } = this;

    svg
      .selectAll('examplePoints')
      .data(examples)
      .enter()
      .append('circle')
      .attr('cx', (c) => this.x(c.industry)!)
      .attr('cy', (c) => this.y(c.esgScores.esgDetail.score.raw))
      .attr('r', 5)
      .attr('fill', 'red');

    svg
      .selectAll('exampleLabels')
      .data(examples)
      .enter()
      .append('text')
      .attr('font-size', '14px')
      .attr('x', (c) => this.x(c.industry)!)
      .attr('y', (c) => this.y(c.esgScores.esgDetail.score.raw))
      .attr('dy', -4)
      .attr('dx', 8)
      .text((d) => d.name);
  }

  private verify = ({ esgScores }: Company) => (
    esgScores && esgScores.esgDetail && esgScores.esgDetail.score && esgScores.esgDetail.score.raw)

  private simplify = ({ name, esgScores, industry }: Company) => ({
    name,
    industry,
    score: esgScores.esgDetail.score.raw,
  })

  render() {
    return (
      <div
        id="esg-plot"
        ref={(ref: HTMLDivElement) => { this.ref = ref; }}
      />
    );
  }
}

export default EsgPlot;
