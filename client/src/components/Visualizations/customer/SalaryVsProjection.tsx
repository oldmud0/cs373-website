/* eslint-disable camelcase */
import React from 'react';
import * as d3 from 'd3';
import request from '../../../utils/request';
import { Loader } from '../../Constants/Constants';

export type CustomerProfession = {
  profession_photo: string;
  profession_video: string;
  profession_education: string;
  profession_description: string;
  profession_name: string;
  profession_major: string;
  profession_projection: number;
  profession_outlook: string;
  profession_median_wage: number;
  id: number;
  profession_best_cities: string;
};

type SalaryVsProjectionProps = {
  height: number;
  width: number;
};

type SalaryVsProjectionState = {
  loaded: boolean;
}

class SalaryVsProjection extends React.Component<SalaryVsProjectionProps, SalaryVsProjectionState> {
  ref!: HTMLDivElement;

  margin: { [k: string]: number };

  height: number;

  width: number;

  constructor(props: SalaryVsProjectionProps) {
    super(props);

    const { height, width } = this.props;

    this.margin = {
      top: 10, right: 30, bottom: 80, left: 80,
    };

    this.height = height - this.margin.top - this.margin.bottom;
    this.width = width - this.margin.left - this.margin.right;

    this.state = {
      loaded: false,
    };
  }

  async componentDidMount() {
    const { margin, height, width } = this;

    const professions = await this.fetchProfessions();
    const indices: CustomerProfession[] = professions.filter(({
      profession_projection, profession_median_wage,
    }) => profession_projection > 0 && profession_median_wage > 0);

    this.setState({
      loaded: true,
    });

    const svg = d3.select(this.ref)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform',
        `translate(${margin.left},${margin.top})`);

    // Add X axis
    const x = d3.scaleLinear()
      .domain([
        0,
        d3.max(indices.map((p) => p.profession_median_wage)) || 100000,
      ])
      .range([0, width]);
    svg.append('g')
      .attr('transform', `translate(0,${height})`)
      .call(d3.axisBottom(x));

    // Add Y axis
    const y = d3.scaleLinear()
      .domain([
        0,
        d3.max(indices.map((p) => p.profession_projection)) || 20000,
      ])
      .range([height, 0]);
    svg.append('g')
      .call(d3.axisLeft(y));

    // Add dots
    svg.selectAll('dot')
      .data(indices)
      .enter()
      .append('circle')
      .attr('cx', (d) => x(d.profession_median_wage))
      .attr('cy', (d) => y(d.profession_projection))
      .attr('r', 3.5)
      .style('fill', '#69b3a2');

    // Axes labels
    svg.append('text')
      .attr('transform',
        `translate(${width / 2} ,${
          height + margin.top + 40})`)
      .style('text-anchor', 'middle')
      .text('Median salary');

    svg.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 0 - margin.left)
      .attr('x', 0 - (height / 2))
      .attr('dy', '1em')
      .style('text-anchor', 'middle')
      .text('Projected job increase/decrease');
  }

  private fetchProfessions = async () => {
    const professions = await Promise.all([1, 2, 3, 4, 5]
      .map((i) => request('https://api.whereyouwannabe.org/professions', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ page: i }),
      })));

    return professions.flat();
  }

  render() {
    const { loaded } = this.state;

    if (!loaded) return <Loader />;

    return (
      <>
        <h3>Salary versus projected job growth per profession</h3>
        <div
          id="salary-vs-projection"
          ref={(ref: HTMLDivElement) => { this.ref = ref; }}
        />
      </>
    );
  }
}

export default SalaryVsProjection;
