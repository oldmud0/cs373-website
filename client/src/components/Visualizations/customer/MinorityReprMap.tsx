/* eslint-disable camelcase */
import React from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson-client';
import request from '../../../utils/request';
import { Loader } from '../../Constants/Constants';
import FIPSCodes from './FIPSCodes';

type CustomerUniversity = {
  academic_year_cost: number;
  admission_rate: number;
  city_name: string;
  demographics_asian: number;
  demographics_black: number;
  demographics_hispanic: number;
  demographics_men: number;
  demographics_white: number;
  demographics_women: number;
  id: number;
  photo: string;
  state_name: string;
  student_size: number;
  top_major: string;
  university_name: string;
}

type MinorityReprMapProps = {
  height: number;
  width: number;
}

type MinorityReprMapState = {
  loaded: boolean;
}

class MinorityReprMap extends React.Component
  <MinorityReprMapProps, MinorityReprMapState> {
  ref!: SVGSVGElement;

  margin!: { [k: string]: number }

  height!: number;

  width!: number;

  projection!: d3.GeoProjection;

  getProjected: (lat: number, long: number) => number[];

  constructor(props: MinorityReprMapProps) {
    super(props);
    const { height, width } = props;

    this.margin = {
      top: 10, right: 30, bottom: 40, left: 180,
    };

    this.height = height - this.margin.top - this.margin.bottom;
    this.width = width - this.margin.left - this.margin.right;

    this.projection = d3.geoAlbersUsa().scale(1265).translate([480, 300]);
    this.getProjected = (latitude: number, longitude: number) => {
      const proj = this.projection([longitude, latitude]);
      return proj || [0, 0];
    };

    this.state = {
      loaded: false,
    };
  }

  async componentDidMount() {
    try {
      const universities = await this.fetchUniversities();
      this.setState({
        loaded: true,
      });

      const shape = await d3.json('https://unpkg.com/us-atlas@1/us/10m.json');

      const svg = d3.select(this.ref);
      const g = svg.append('g');
      const states: {id: string}[] = topojson.feature(shape, shape.objects.states).features;
      const path: any = d3.geoPath();

      // Set up zoom
      const zoomed = () => {
        svg
          .attr('transform', d3.event.transform);
      };
      const zoom = d3.zoom().scaleExtent([1, 4]).on('zoom', zoomed);

      g
        .attr('pointer-events', 'none')
        .attr('class', 'states')
        .selectAll('path')
        .data(states)
        .enter()
        .append('path')
        .attr('d', path)
        .style('fill', (state) => {
          const mean = d3.mean(universities
            .filter((uni) => uni.state_name === FIPSCodes[state.id])
            .map((uni) => 1 - uni.demographics_white));
          if (!mean) return 'gray';
          return `hsl(${mean * 100}, 100%, 66%)`;
        });

      (svg as d3.Selection<any, unknown, any, any>).call(zoom);
    } catch (e) {
      console.error(e);
    }
  }

  private handleSvgScroll = (e: React.UIEvent<SVGSVGElement>) => {
    e.preventDefault();
  }

  private fetchUniversities: () => Promise<CustomerUniversity[]> = async () => {
    const universities = await Promise.all([1, 2, 3, 4, 5, 6, 7]
      .map((i) => request('https://api.whereyouwannabe.org/universities', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ page: i }),
      })));

    return universities.flat();
  }

  render() {
    const { height, width } = this;
    const { loaded } = this.state;

    if (!loaded) return <Loader />;

    return (
      <div style={{ overflow: 'hidden', margin: '25px 0' }}>
        <h3>Mean representation of non-white populations in universities</h3>
        <svg
          ref={(ref: SVGSVGElement) => { this.ref = ref; }}
          height={height}
          width={width}
          onScroll={this.handleSvgScroll}
        />
        <br />
        100% white&nbsp;
        <span
          style={{
            display: 'inline-block',
            border: '1px solid black',
            background: 'linear-gradient(to right, '
              + 'hsl(0, 100%, 66%) 0%, hsl(0, 100%, 66%) 20%, '
              + 'hsl(25, 100%, 66%) 20%, hsl(25, 100%, 66%) 40%, '
              + 'hsl(50, 100%, 66%) 40%, hsl(50, 100%, 66%) 60%, '
              + 'hsl(75, 100%, 66%) 20%, hsl(75, 100%, 66%) 80%, '
              + 'hsl(100, 100%, 66%) 80%, hsl(100, 100%, 66%) 100%) ',
            width: '6em',
          }}
        >
          &nbsp;
        </span>
        &nbsp;100% non-white
      </div>
    );
  }
}

export default MinorityReprMap;
