/* eslint-disable camelcase */
import React from 'react';
import * as d3 from 'd3';
import request from '../../../utils/request';
import { Loader } from '../../Constants/Constants';

export type CustomerCity = {
  city_name: string;
  climate_index: string;
  crime_index: string;
  health_care_index: string;
  id: number;
  major: any;
  photo: string;
  pollution_index: string;
  rent_index: string;
};

type ClimateVsPollutionProps = {
  height: number;
  width: number;
};

type ClimateVsPollutionState = {
  loaded: boolean;
}

class ClimateVsPollution extends React.Component<ClimateVsPollutionProps, ClimateVsPollutionState> {
  ref!: HTMLDivElement;

  margin: { [k: string]: number };

  height: number;

  width: number;

  constructor(props: ClimateVsPollutionProps) {
    super(props);

    const { height, width } = this.props;

    this.margin = {
      top: 10, right: 30, bottom: 80, left: 50,
    };

    this.height = height - this.margin.top - this.margin.bottom;
    this.width = width - this.margin.left - this.margin.right;

    this.state = {
      loaded: false,
    };
  }

  async componentDidMount() {
    const { margin, height, width } = this;

    const cities = await this.fetchCities();
    const indices = this
      .getIndices(cities)
      .filter(({ climate, pollution }) => climate > 0 && pollution > 0);

    this.setState({
      loaded: true,
    });

    const svg = d3.select(this.ref)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform',
        `translate(${margin.left},${margin.top})`);

    // Add X axis
    const x = d3.scaleLinear()
      .domain([
        0,
        100,
      ])
      .range([0, width]);
    svg.append('g')
      .attr('transform', `translate(0,${height})`)
      .call(d3.axisBottom(x));

    // Add Y axis
    const y = d3.scaleLinear()
      .domain([
        0,
        100,
      ])
      .range([height, 0]);
    svg.append('g')
      .call(d3.axisLeft(y));

    // Add dots
    svg.selectAll('dot')
      .data(indices)
      .enter()
      .append('circle')
      .attr('cx', (d) => x(d.pollution))
      .attr('cy', (d) => y(d.climate))
      .attr('r', 3.5)
      .style('fill', '#69b3a2');

    // Axes labels
    svg.append('text')
      .attr('transform',
        `translate(${width / 2} ,${
          height + margin.top + 40})`)
      .style('text-anchor', 'middle')
      .text('Pollution index');

    svg.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 0 - margin.left)
      .attr('x', 0 - (height / 2))
      .attr('dy', '1em')
      .style('text-anchor', 'middle')
      .text('Climate index');
  }

  private fetchCities = async () => {
    const cities = await Promise.all([1, 2, 3, 4, 5]
      .map((i) => request('https://api.whereyouwannabe.org/cities', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ page: i }),
      })));

    return cities.flat();
  }

  private getIndices = (cities: CustomerCity[]) => cities.map((city) => ({
    id: city.id,
    climate: parseFloat(city.climate_index),
    pollution: parseFloat(city.pollution_index),
  }))

  render() {
    const { loaded } = this.state;

    if (!loaded) return <Loader />;

    return (
      <>
        <h3>Climate index vs pollution index per city</h3>
        <div
          id="climate-vs-health"
          ref={(ref: HTMLDivElement) => { this.ref = ref; }}
        />
      </>
    );
  }
}

export default ClimateVsPollution;
