/* eslint-disable prefer-arrow-callback */
/* eslint-disable func-names */
import React from 'react';
import * as d3 from 'd3';
import Company from '../../types/Company';

type CompaniesByIndustryProps = {
  companies: Company[];
  height: number;
  width: number;
}

class CompaniesByIndustry extends React.Component<CompaniesByIndustryProps, {}> {
  ref!: HTMLDivElement;

  margin: {[k: string]: number};

  height: number;

  width: number;

  constructor(props: CompaniesByIndustryProps) {
    super(props);

    const { height, width } = this.props;

    this.margin = {
      top: 10, right: 30, bottom: 40, left: 180,
    };

    this.height = height - this.margin.top - this.margin.bottom;
    this.width = width - this.margin.left - this.margin.right;
  }

  componentDidMount() {
    const { labels, counts } = this.getIndustryCounts();

    const { margin, height, width } = this;

    const svg = d3.select(this.ref)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform',
        `translate(${margin.left},${margin.top})`);

    const x = d3.scaleLinear()
      .domain([0, Math.max(...counts.map((c) => c.count)) + 2])
      .range([0, width]);
    svg.append('g')
      .attr('transform', `translate(0,${height})`)
      .call(d3.axisBottom(x))
      .selectAll('text')
      .attr('transform', 'translate(-10,0)rotate(-45)')
      .attr('font-size', '14px')
      .style('text-anchor', 'end');

    // Y axis
    const y = d3.scaleBand()
      .range([0, height])
      .domain(labels)
      .padding(1);
    svg.append('g')
      .call(d3.axisLeft(y))
      .selectAll('text')
      .attr('font-size', '14px');


    // Lines
    svg.selectAll('myline')
      .data(counts)
      .enter()
      .append('line')
      .attr('x1', function (d) { return x(d.count); })
      .attr('x2', x(0))
      .attr('y1', function (d) { return y(d.industry) || 'Unspecified'; })
      .attr('y2', function (d) { return y(d.industry) || 'Unspecified'; })
      .attr('stroke', 'grey');

    // Circles
    svg.selectAll('mycircle')
      .data(counts)
      .enter()
      .append('circle')
      .attr('cx', function (d) { return x(d.count) || 'Unspecified'; })
      .attr('cy', function (d) { return y(d.industry) || 'Unspecified'; })
      .attr('r', '4')
      .style('fill', '#69b3a2')
      .attr('stroke', 'black');
  }

  private getIndustryCounts = () => {
    const { companies } = this.props;

    const counts = companies.reduce((acc: {[k: string]: number}, company) => {
      if (!acc[company.industry]) acc[company.industry] = 1;
      else acc[company.industry] += 1;
      return acc;
    }, {});

    const keys = Object.keys(counts)
      .sort((k1, k2) => counts[k2] - counts[k1]);

    const data = keys.map((key) => ({
      industry: key,
      count: counts[key],
    }));

    return {
      labels: keys,
      counts: data,
    };
  }

  render() {
    return (
      <>
        <h3>Number of companies per industry</h3>
        <div
          id="companies-per-industry"
          ref={(ref: HTMLDivElement) => { this.ref = ref; }}
        />
      </>
    );
  }
}

export default CompaniesByIndustry;
