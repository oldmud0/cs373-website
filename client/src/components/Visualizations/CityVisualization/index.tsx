import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import styled from 'styled-components';
import City from '../../../types/City';
import CityMap from './CityMap';

const DropdownWrapper = styled.div`
  display: flex;
  align-items: center;

  span {
    margin-right: 15px;
  }
`;

type CityVisualizationProps = {
  height: number;
  width: number;
  cities: City[];
}

type CityVisualizationState = {
  metricFilter: string;
  cities: City[];
}

const FILTER_OPTIONS = [
  'All cities',
  'Cost of Living',
  'Commute',
  'Housing',
  'Startups',
  'Education',
  'Healthcare',
  'Tolerance',
  'Leisure & Culture',
];

class CityVisualization extends React.Component
  <CityVisualizationProps, CityVisualizationState> {
  constructor(props: CityVisualizationProps) {
    super(props);
    const { cities } = props;

    this.state = {
      metricFilter: 'All cities',
      cities: cities.map((city) => ({
        ...city,
        active: true,
        metricsMap: city.metrics.reduce((acc: any, metric) => {
          acc[metric.name] = metric.scoreOutOf10;
          return acc;
        }, {}),
      })),
    };
  }

  private handleDropdownChange = (eventKey: string) => {
    const { metricFilter, cities } = this.state;
    if (eventKey === metricFilter) return;
    if (eventKey === 'All cities') {
      this.setState({
        cities: cities.map((city) => ({ ...city, active: true })),
        metricFilter: eventKey,
      });
      return;
    }

    const newTopCityIds = cities
      .sort((a, b) => b.metricsMap[eventKey] - a.metricsMap[eventKey])
      .slice(0, 10)
      .map((city) => city.id);

    this.setState({
      metricFilter: eventKey,
      cities: cities.map((city) => (
        newTopCityIds.includes(city.id) ? { ...city, active: true } : { ...city, active: false }
      )),
    });
  }

  render() {
    const { height, width } = this.props;
    const { metricFilter, cities } = this.state;

    return (
      <>
        <h3>City Visualization</h3>
        <DropdownWrapper>
          <span>Filter top 10 cities by metric:</span>
          <Dropdown>
            <Dropdown.Toggle size="sm" variant="primary" id="dropdown-basic">
              {metricFilter}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              {FILTER_OPTIONS.map((opt) => (
                <Dropdown.Item
                  key={opt}
                  eventKey={opt}
                  onSelect={this.handleDropdownChange}
                >
                  {opt}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </DropdownWrapper>
        <CityMap cities={cities.filter((c) => c.active)} width={width} height={height} />
      </>
    );
  }
}

export default CityVisualization;
