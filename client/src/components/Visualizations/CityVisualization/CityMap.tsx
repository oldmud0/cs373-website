import React from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson-client';
import City from '../../../types/City';

type CityMapProps = {
  height: number;
  width: number;
  cities: City[];
}

class CityMap extends React.Component
  <CityMapProps, {}> {
  ref!: SVGSVGElement;

  margin!: { [k: string]: number }

  height!: number;

  width!: number;

  projection!: d3.GeoProjection;

  getProjected: (lat: number, long: number) => number[];

  constructor(props: CityMapProps) {
    super(props);
    const { height, width } = props;

    this.margin = {
      top: 10, right: 30, bottom: 40, left: 180,
    };

    this.height = height - this.margin.top - this.margin.bottom;
    this.width = width - this.margin.left - this.margin.right;

    this.projection = d3.geoAlbersUsa().scale(1265).translate([480, 300]);
    this.getProjected = (latitude: number, longitude: number) => {
      const proj = this.projection([longitude, latitude]);
      return proj || [0, 0];
    };
  }

  async componentDidMount() {
    const { cities } = this.props;

    try {
      const shape = await d3.json('https://unpkg.com/us-atlas@1/us/10m.json');

      const svg = d3.select(this.ref);
      const g = svg.append('g');
      const states = topojson.feature(shape, shape.objects.states).features;
      const path: any = d3.geoPath();

      // Set up zoom
      const zoomed = () => {
        svg
          .attr('transform', d3.event.transform);
      };
      const zoom = d3.zoom().scaleExtent([1, 4]).on('zoom', zoomed);

      g
        .attr('pointer-events', 'none')
        .attr('class', 'states')
        .selectAll('path')
        .data(states)
        .enter()
        .append('path')
        .attr('d', path)
        .style('fill', 'gray');

      (svg as d3.Selection<any, unknown, any, any>).call(zoom);

      this.drawCities();
    } catch (e) {
      console.log(e);
    }
  }

  componentDidUpdate(prevProps: CityMapProps) {
    this.drawCities();
  }

  private drawCities = () => {
    const { cities } = this.props;

    const cityPoints = cities
      .map(({
        name, latlon, id,
      }) => ({
        latlon: this.getProjected(latlon.latitude, latlon.longitude),
        name,
        id,
      }));

    const svg = d3.select(this.ref);
    const circles = svg.selectAll('circle').data(cityPoints);
    const labels = svg.selectAll('text').data(cityPoints);

    const newCircles = circles.enter();
    const newLabels = labels.enter();

    circles
      .attr('cx', (d) => d.latlon[0])
      .attr('cy', (d) => d.latlon[1])
      .attr('r', '5px')
      .attr('fill', 'lightblue');

    labels
      .attr('transform', (d) => `translate(${d.latlon})`)
      .attr('dx', 8)
      .attr('dy', 3)
      .attr('fill', 'black')
      .attr('font-size', '14px')
      .text((d) => d.name);

    newCircles
      .append('circle')
      .attr('cx', (d) => d.latlon[0])
      .attr('cy', (d) => d.latlon[1])
      .attr('r', '5px')
      .attr('fill', 'lightblue');

    newLabels
      .append('text')
      .attr('transform', (d) => `translate(${d.latlon})`)
      .attr('dx', 8)
      .attr('dy', 3)
      .attr('fill', 'black')
      .attr('font-size', '14px')
      .text((d) => d.name);

    circles
      .exit()
      .remove();

    labels
      .exit()
      .remove();
  }

  private handleSvgScroll = (e: React.UIEvent<SVGSVGElement>) => {
    e.preventDefault();
  }

  render() {
    const { height, width } = this;

    return (
      <div style={{ overflow: 'hidden', margin: '25px 0' }}>
        <svg
          ref={(ref: SVGSVGElement) => { this.ref = ref; }}
          height={height}
          width={width}
          onScroll={this.handleSvgScroll}
        />
      </div>
    );
  }
}

export default CityMap;
