import React from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';

const NAVBAR_HEIGHT = 70;

const CustomNavbar: React.FunctionComponent = () => (
  <Navbar style={{ height: `${NAVBAR_HEIGHT}px` }}>
    <Navbar.Brand as={Link} to="/">Ethical Employers</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Item>
        <Nav.Link as={Link} to="/search">Search</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link as={Link} to="/companies">Companies</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link as={Link} to="/cities">Cities</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link as={Link} to="/transit">Transit Routes</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link as={Link} to="/visualizations">Visualizations</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link as={Link} to="/about">About</Nav.Link>
      </Nav.Item>
    </Nav>
  </Navbar>
);

export default CustomNavbar;
