/* eslint-disable react/no-danger */
import React from 'react';
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import SearchResultWrapper from './SearchResultWrapper';
import highlight from '../../utils/highlight';
import Company from '../../types/Company';

type CompanySearchResultProps = {
  company: Company;
  query: string;
};

const CompanySearchResult: React.FC<CompanySearchResultProps> = ({ company, query }) => (
  <SearchResultWrapper>
    <Card>
      <Card.Body>
        <a href={`/companies/${company.id}`}>
          <Card.Title
            dangerouslySetInnerHTML={{
              __html: highlight(company.name, query),
            }}
          />
        </a>
        <Card.Subtitle>
          <div
            dangerouslySetInnerHTML={{
              __html: highlight(company.industry, query),
            }}
          />
          <Badge variant="secondary">Company</Badge>
          <div
            className="search-highlight"
            dangerouslySetInnerHTML={{
              __html: company.headline || '',
            }}
          />
        </Card.Subtitle>
      </Card.Body>
    </Card>
  </SearchResultWrapper>
);

export default CompanySearchResult;
