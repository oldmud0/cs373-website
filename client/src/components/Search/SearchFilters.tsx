import React from 'react';
import Nav from 'react-bootstrap/Nav';

type SearchFiltersProps = {
  active: string;
  onSelect: (ek: string, ev: React.SyntheticEvent<any, Event>) => void;
}

const SearchFilters: React.FC<SearchFiltersProps> = ({ active, onSelect }) => (
  <Nav variant="pills" defaultActiveKey="all" style={{ marginBottom: '25px' }}>
    <Nav.Item>
      <Nav.Link
        active={active === 'all'}
        eventKey="all"
        onSelect={onSelect}
      >
        Show All
      </Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link
        active={active === 'companies'}
        eventKey="companies"
        onSelect={onSelect}
      >
        Companies
      </Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link
        active={active === 'cities'}
        eventKey="cities"
        onSelect={onSelect}
      >
        Cities
      </Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link
        active={active === 'routes'}
        eventKey="routes"
        onSelect={onSelect}
      >
        Transit Routes
      </Nav.Link>
    </Nav.Item>
  </Nav>
);

export default SearchFilters;
