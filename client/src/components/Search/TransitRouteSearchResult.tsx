/* eslint-disable react/no-danger */
import React from 'react';
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import SearchResultWrapper from './SearchResultWrapper';
import highlight from '../../utils/highlight';
import TransitRoute from '../../types/TransitRoute';

type TransitRouteSearchResultProps = {
  route: TransitRoute;
  query: string;
};

const RouteSearchResult: React.FC<TransitRouteSearchResultProps> = ({ route, query }) => (
  <SearchResultWrapper>
    <Card>
      <Card.Body>
        <a href={`/transit/${route.id}`}>
          <Card.Title
            dangerouslySetInnerHTML={{
              __html: highlight(route.name, query),
            }}
          />
        </a>
        <Card.Subtitle>
          <div
            dangerouslySetInnerHTML={{
              __html: highlight(route.city.name, query),
            }}
          />
          <Badge variant="secondary">Transit Route</Badge>
          <div
            className="search-highlight"
            dangerouslySetInnerHTML={{
              __html: route.headline || '',
            }}
          />
        </Card.Subtitle>
      </Card.Body>
    </Card>
  </SearchResultWrapper>
);

export default RouteSearchResult;
