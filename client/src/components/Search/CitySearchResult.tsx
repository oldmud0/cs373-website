/* eslint-disable react/no-danger */
import React from 'react';
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import City from '../../types/City';
import SearchResultWrapper from './SearchResultWrapper';
import highlight from '../../utils/highlight';

type CitySearchResultProps = {
  city: City;
  query: string;
};

const CitySearchResult: React.FC<CitySearchResultProps> = ({ city, query }) => (
  <SearchResultWrapper>
    <Card>
      <Card.Body>
        <a href={`/cities/${city.id}`}>
          <Card.Title
            dangerouslySetInnerHTML={{
              __html: highlight(city.name, query),
            }}
          />
        </a>
        <Card.Subtitle>
          <div
            dangerouslySetInnerHTML={{
              __html: highlight(city.country, query),
            }}
          />
          <Badge variant="secondary">City</Badge>
          <div
            className="search-highlight"
            dangerouslySetInnerHTML={{
              __html: city.headline || '',
            }}
          />
        </Card.Subtitle>
      </Card.Body>
    </Card>
  </SearchResultWrapper>
);

export default CitySearchResult;
