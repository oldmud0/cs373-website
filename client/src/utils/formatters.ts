import City, { Coordinate } from '../types/City';
import Company, {
  ESGScoreObject, ESGScoreDetail, ControversyDetail,
} from '../types/Company';
import TransitRoute from '../types/TransitRoute';
import EntryValues from '../types/EntryValues';

const cityFormatter = (city: City): EntryValues => {
  const {
    id, name, country, latlon, population, overallScore,
  } = city;

  return {
    name,
    values: [
      country,
      `(${latlon.latitude}, ${latlon.longitude})`,
      population.toLocaleString(),
      overallScore.toFixed(2),
    ],
    path: `/cities/${id}`,
  };
};

const companyFormatter = (company: Company): EntryValues => {
  const {
    id, name, esgScores, industry,
  } = company;
  const {
    environmentDetail, governanceDetail, socialDetail, esgDetail,
  } = esgScores;

  if (!environmentDetail.score) {
    return {
      name,
      values: [],
      path: `/companies/${id}`,
    };
  }

  return {
    name,
    values: [
      industry,
      environmentDetail.score.raw.toFixed(2),
      governanceDetail.score.raw.toFixed(2),
      socialDetail.score.raw.toFixed(2),
      esgDetail.score.raw.toFixed(2),
    ],
    path: `/companies/${id}`,
  };
};

const routeFormatter = (route: TransitRoute) => {
  const {
    id, name, city, vehicleType, bikesAllowed, wheelchairAccessible,
  } = route;

  return {
    name,
    values: [
      city.name,
      vehicleType,
      bikesAllowed,
      wheelchairAccessible,
    ],
    path: `/transit/${id}`,
  };
};

export { cityFormatter, companyFormatter, routeFormatter };
