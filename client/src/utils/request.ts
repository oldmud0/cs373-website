import { SortOrder } from '../components/Sorting/SortableTableHeader';

/**
 * Wrapper for ES6 fetch method. Example usage:
 *
 *      import request from './utils/request';
 *      try {
 *          const data = await request('https://api.example.com');
 *      } catch(e) {
 *          console.log(e.status);
 *      }
 *
 * @param url request url
 * @param opts see https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch
 */
const request = async (url: string, opts?: object) => {
  const response = await fetch(url, opts);
  const json = await response.json();

  if (response.ok) { return json; }

  const error = {
    ...json,
    status: response.status,
  };

  return error;
};

export type QueryParams = {
  page?: number;
  size?: number;
  sortBy?: string;
  sortOrder?: number;
  filters?: { [k: string]: string[]; };
  search?: string;
  [index: string]: any;
}

const defaultQueryParams: QueryParams = {
  size: 15,
  page: 0,
};

const parseParamsToString = (params: QueryParams) => {
  const queryParams: QueryParams = {
    ...defaultQueryParams,
    ...params,
  };

  const {
    sortBy, sortOrder, filters, search,
  } = queryParams;

  if (sortBy) {
    queryParams.sort = `${sortBy},${sortOrder === SortOrder.ASC ? 'ASC' : 'DESC'}`;
    delete queryParams.sortBy;
    delete queryParams.sortOrder;
  }

  let filterString = '';
  if (filters) {
    filterString = Object.keys(filters)
      .filter((key) => filters[key].length)
      .map((key) => filters[key].map((option) => `${key}=${option}`).join('&')).join('&');
    delete queryParams.filters;
  }

  let searchString = '';
  if (search) {
    searchString = search.split(' ').join('%7C');
    delete queryParams.search;
  }

  const options = Object.keys(queryParams).map((key) => `${key}=${queryParams[key]}`).join('&');

  return options
    + (filterString ? `&${filterString}` : '')
    + (searchString ? `&content=${searchString}` : '');
};

const getPage = async <T extends unknown> (
  url: string,
  query: {[key: string]: any},
  parser?: (response: any) => T,
) => {
  const querystring = parseParamsToString(query);
  const page = await request(
    `${url}?${querystring}`,
    { mode: 'cors', method: 'GET' },
  );

  if (parser) {
    const content: T[] = [];
    page.content.map((entry: any) => content.push(parser(entry)));
    const newPage: any = page;
    newPage.content = content;
    return newPage;
  }
  return page;
};

const instanceRequest = async <T extends unknown> (
  url: string,
  id: string,
  parser?: (response: any) => T,
) => {
  const response = await request(`${url}?id=${id}`, { mode: 'cors', method: 'GET' });

  if (parser) { return parser(response); }
  return response;
};

export {
  request, getPage, instanceRequest,
};

export default request;
