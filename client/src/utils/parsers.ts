import City, { CityMetric, Coordinate } from '../types/City';
import Company, {
  Address, ESGScoreObject, ESGScoreDetail, ControversyDetail,
} from '../types/Company';
import TransitRoute from '../types/TransitRoute';

const cityParser = (response : any): City => {
  const city: City = response;
  const latlon: Coordinate = JSON.parse(response.latlon);
  const metrics: CityMetric[] = JSON.parse(response.metrics);

  city.latlon = latlon;
  city.metrics = metrics;
  return city;
};

const addressParser = (address: string): Address => {
  const components: string[] = address.split(', ');
  return {
    street: components[0],
    city: components[1],
    state: components[2],
    country: components[3],
  };
};

const esgDetailParser = (json: any, keys: string[]): ESGScoreDetail => ({
  score: json[keys[0]],
  percentile: json[keys[1]],
  performance: json[keys[2]],
} as ESGScoreDetail);

const controversyDetailParser = (json: any, keys: string[]): ControversyDetail => ({
  highestControversy: json[keys[0]],
  performance: json[keys[1]],
  relatedControversy: (json[keys[2]]) ? json[keys[2]] : [],
} as ControversyDetail);

const jsonDeleteFields = (json: any, ...keyArrays: string[][]): any => {
  const newJson: any = json;
  keyArrays.forEach((keys) => {
    keys.forEach((key) => {
      delete newJson[key];
    });
  });

  return newJson;
};

const esgScoresParser = (json: any): ESGScoreObject => {
  const environmentKeys: string[] = ['environmentScore', 'environmentPercentile', 'peerEnvironmentPerformance'];
  const governanceKeys: string[] = ['governanceScore', 'governancePercentile', 'peerGovernancePerformance'];
  const socialKeys: string[] = ['socialScore', 'socialPercentile', 'peerSocialPerformance'];
  const esgKeys: string[] = ['totalEsg', 'percentile', 'peerEsgScorePerformance'];
  const controversyKeys: string[] = ['highestControversy', 'peerHighestControversyPerformance', 'relatedControversy'];

  const environmentDetail: ESGScoreDetail = esgDetailParser(json, environmentKeys);
  const governanceDetail: ESGScoreDetail = esgDetailParser(json, governanceKeys);
  const socialDetail: ESGScoreDetail = esgDetailParser(json, socialKeys);
  const esgDetail: ESGScoreDetail = esgDetailParser(json, esgKeys);
  const controversyDetail: ControversyDetail = controversyDetailParser(json, controversyKeys);

  const newJson: any = jsonDeleteFields(json, environmentKeys, governanceKeys,
    socialKeys, esgKeys, controversyKeys);

  const esgScores: ESGScoreObject = newJson;

  esgScores.environmentDetail = environmentDetail;
  esgScores.governanceDetail = governanceDetail;
  esgScores.socialDetail = socialDetail;
  esgScores.esgDetail = esgDetail;
  esgScores.controversyDetail = controversyDetail;

  return esgScores;
};

const companyParser = (response: any): Company => {
  const company: Company = response;
  const address: Address = addressParser(response.address);

  company.address = address;
  company.nemployees = Number(response.nemployees);
  company.esgScores = esgScoresParser(JSON.parse(response.esgScores));
  return company;
};

const transitRouteParser = (response: any): TransitRoute => {
  const route: TransitRoute = response;

  return route;
};

export { cityParser, companyParser, transitRouteParser };
