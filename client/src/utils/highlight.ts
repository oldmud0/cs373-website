const highlight = (text: string, search?: string) => {
  if (!search) return text;

  const searchTokens = search.split(' ');
  const re = new RegExp(`(${[search, ...searchTokens].join('|')})`, 'gmi');

  return text.replace(re, '<b class="search-highlighted">$1</b>');
};

export default highlight;
