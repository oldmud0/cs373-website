# CS373: Software Engineering IDB Project

URL: https://ethicalemployers.club

## Members
* William Li (Phase IV project leader)
  * EID: wzl62
  * Gitlab ID: williamli1
* Bennett Ramirez (Phase II project leader)
  * EID: bar2954
  * GitLab ID: oldmud0
* Spencer Sharp (Phase I project leader)
  * EID: sps2624
  * GitLab ID: SpencerSharp
* Cheng Tan
  * EID: ct28329
  * Gitlab ID: d8tltanc
* Michael Yu (Phase III project leader)
  * EID: smy447
  * Gitlab ID: mi-yu1

## GitLab info
* Git SHA: 151bee061b1f4f23d761d064b145b2f4a412273c
* GitLab Pipelines: https://gitlab.com/oldmud0/cs373-website/pipelines

## Completion times

### Phase I

* William Li
  * Estimated completion time: 8
  * Actual completion time: 5
* Bennett Ramirez
  * Estimated completion time: 8
  * Actual completion time: 6.5
* Spencer Sharp (Project Lead)
  * Estimated completion time: 10
  * Actual completion time: 11.5
* Cheng Tan
  * Estimated completion time: 6
  * Actual completion time: 5
* Michael Yu
  * Estimated completion time: 5
  * Actual completion time: 9

### Phase II

* William Li
  * Estimated completion time: 10
  * Actual completion time: 15
* Bennett Ramirez (Project Lead)
  * Estimated completion time: 8
  * Actual completion time: 7
* Spencer Sharp
  * Estimated completion time: 12
  * Actual completion time: 14.25
* Cheng Tan
  * Estimated completion time: 15
  * Actual completion time: 20
* Michael Yu
  * Estimated completion time: 8
  * Actual completion time: 20

### Phase III

* William Li
  * Estimated completion time: 8
  * Actual completion time: 7
* Bennett Ramirez
  * Estimated completion time: 8
  * Actual completion time: 6
* Spencer Sharp
  * Estimated completion time: 6
  * Actual completion time: 5.25
* Cheng Tan
  * Estimated completion time: 10
  * Actual completion time: 11.5
* Michael Yu (project lead)
  * Estimated completion time: 15
  * Actual completion time: 12

### Phase IV

* William Li (project lead)
  * Estimated completion time: 5
  * Actual completion time: 6
* Bennett Ramirez
  * Estimated completion time: 4
  * Actual completion time: 5
* Spencer Sharp
  * Estimated completion time: 3
  * Actual completion time: 4.5
* Cheng Tan
  * Estimated completion time: 8
  * Actual completion time: 9
* Michael Yu
  * Estimated completion time: 4
  * Actual completion time: 7

## Comments

Frontend deployment and serving is performed automatically through Netlify.

Cold start time for Google App Engine takes about 45 seconds; please be patient.

## Docker setup (for local development)

1. Install [Docker](https://www.docker.com/products/docker-desktop)
2. Set the following environment variables in your shell:

```
export PSQL_DB_HOST=<db host>
export PSQL_DB_USER=<db user>
export PSQL_DB_PASS=<db password>
```
3. Generate api JAR, run from project root
```
cd api/spring
./mvnw clean install spring-boot:repackage -DskipTests
```

4. In the project root run:
```
make build-docker
make start
```

See `Makefile` for a list of useful commands.

5. Visit http://localhost:3000 in browser, window should automatically reload on client code change.

The following is not required but nice to have:

6. To get the auto-restart behavior for API changes, open the `./api/spring` folder in IntelliJ and create the following run configuration (choose "Application" when creating new configuration):

![run config](./docs/README/images/run-config.png)

7. In IntelliJ go to Preferences (`⌘+,`) and go to `Build, Execution, Deployment > Compiler`. Enable `Build project automatically`. Also press `⌘+Shift+A` and search for `Registry`. Find `compiler.automake.allow.when.app.running` and enable it.

8. Run the "Run Remote" configuration. The API container should reload the JAR on code change.
