build-docker:
	docker-compose -f docker-compose-dev.yml build

start:
	docker-compose -f docker-compose-dev.yml up -d

stop:
	docker-compose -f docker-compose-dev.yml down -v

restart:
	docker-compose -f docker-compose-dev.yml down -v
	docker-compose -f docker-compose-dev.yml up --build -d

api-logs:
	docker-compose -f docker-compose-dev.yml logs -f api
